#include "rt/coordmappers/world.h"
#include "rt/solids/solid.h"

namespace rt {
	Solid::Solid(CoordMapper* texMapper, Material* material)
	:material(material), texMapper(texMapper), hasDefaultMapper(false)
	{
		if (texMapper == nullptr) {
			this->texMapper = new WorldMapper(Float4::rep(1));
			hasDefaultMapper = true;
		}
	}

	Solid::~Solid() {
		if (hasDefaultMapper) {
			delete texMapper;
		}
	}
}