#include "core/matrix.h"
#include "rt/solids/triangle.h"
#include "rt/textures/imagetex.h"
#include "rt/bbox.h"
#include "rt/intersection.h"
#include "nmap.h"
#include "core/matrix.h"
#include "core/float4.h"

namespace rt {
	NormalMapper::NormalMapper(Triangle* base, Texture* normalmap, const Point& bv1, const Point& bv2, const Point& bv3)
	:m_base(base), m_normalmap(normalmap), m_bv1(bv1), m_bv2(bv2), m_bv3(bv3)
	{}

	BBox NormalMapper::getBounds() const {
		return m_base->getBounds();
	}

	Intersection NormalMapper::intersect(const Ray& ray, float previousBestDistance) const {
		Intersection is = m_base->intersect(ray, previousBestDistance);
		if (is && m_normalmap != NULL) {
			Point barrCoords = is.local();
			Point texCoord(barrCoords.x * m_bv1.x + barrCoords.y * m_bv2.x + barrCoords.z * m_bv3.x, barrCoords.x * m_bv1.y + barrCoords.y * m_bv2.y + barrCoords.z * m_bv3.y, barrCoords.x * m_bv1.z + barrCoords.y * m_bv2.z + barrCoords.z * m_bv3.z);

			RGBColor localPointColor = m_normalmap->getColor(texCoord);
			Vector texNormal = Vector(2*localPointColor.r-1, 2*localPointColor.g-1, 2*localPointColor.b-1).normalize();

			Vector deltaPos1 = m_base->p2 - m_base->p1;
			Vector deltaPos2 = m_base->p3 - m_base->p2;

			Vector deltaUV1 = m_bv2 - m_bv1;
			Vector deltaUV2 = m_bv3 - m_bv1;

			float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
			Vector tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y)*r;
			Vector bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x)*r;
			
			tangent = tangent.normalize();
			bitangent = bitangent.normalize();
			
			tangent = (tangent - is.m_normal * dot(tangent, is.m_normal)).normalize();
			Matrix tangentToModelSpace = Matrix::system(tangent, bitangent, is.m_normal);
		
			Vector newNormal = tangentToModelSpace * texNormal;
			
			is.m_normal = newNormal.normalize();
		}

		return is;
	}

	bool NormalMapper::intersectWithBox(const BBox& bbox) const {
		return m_base->intersectWithBox(bbox);
	}

	void NormalMapper::setMaterial(Material* m) {
		m_base->setMaterial(m);
	}

	void NormalMapper::setCoordMapper(CoordMapper* texMapper) { 
		m_base->setCoordMapper(texMapper);
	}
}