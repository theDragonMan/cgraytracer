#include <iostream>
#include <cmath>
#include "core/assert.h"

#include "rt/ray.h"
#include "rt/bbox.h"
#include "rt/intersection.h"


#include "instance.h"

namespace rt {
	Instance::Instance(Primitive* p) 
	:m_content(p), m_transformation(Matrix::identity())
	{}


	Intersection Instance::intersect(const Ray& ray, float previousBestDistance) const {
		Matrix invTransformation = m_transformation.invert();
		Point transformedOrigin = invTransformation * ray.o;
		Vector transformedDir = invTransformation * ray.d;
		//transformedDir = transformedDir.normalize();
		float transformDistance = previousBestDistance;

		if (previousBestDistance != FLT_MAX) {
			Point transformedHitPoint = invTransformation * ray.getPoint(previousBestDistance);
			transformDistance = (transformedHitPoint - transformedOrigin).length();
		}
		
		Ray transformedRay(transformedOrigin, transformedDir);
		Intersection is =  m_content->intersect(transformedRay, transformDistance);
		if(is) {
			Point hitPoint = m_transformation * is.ray.getPoint(is.distance);
			Vector dir = (hitPoint - ray.o);
			float bestDistance = dir.length();
			is.ray = ray;
			is.distance = bestDistance;
			is.m_normal = invTransformation.transpose() * is.m_normal;
			is.m_normal = is.m_normal.normalize();
		}

		return is;
	}

	bool Instance::intersectWithBox(const BBox& box) const {
		BBox bounds = getBounds();
		Point minNew = max(bounds.min, box.min);
		Point maxNew = min(bounds.max, box.max);

		return minNew.x <= maxNew.x && minNew.y <= maxNew.y && minNew.z <= maxNew.z;
	}

	Primitive* Instance::content() {
		return m_content;
	}

	void Instance::reset() {
		m_transformation = Matrix::identity();
	}

	void Instance::translate(const Vector& t) {
		Matrix translationMat = Matrix::identity();

		translationMat[0][3] += t.x;
		translationMat[1][3] += t.y;
		translationMat[2][3] += t.z;
		m_transformation = product(translationMat, m_transformation);
	}

	void Instance::scale(float scale) {
		Matrix scaleMatrix = Matrix::identity();
		scaleMatrix[0][0] = scale;
		scaleMatrix[1][1] = scale;
		scaleMatrix[2][2] = scale;
		m_transformation = product(scaleMatrix, m_transformation);
	}

	void Instance::scale(const Vector& scale) {
		Matrix scaleMatrix = Matrix::identity();
		scaleMatrix[0][0] = scale.z;
		scaleMatrix[1][1] = scale.y;
		scaleMatrix[2][2] = scale.z;
		m_transformation = product(scaleMatrix, m_transformation);
	}

	void Instance::rotate(const Vector& axis, float angle) {
		BBox transformedBounds = getBounds();
		Vector translateCenter = Vector(0.5 * transformedBounds.min.x + 0.5 * transformedBounds.max.x, transformedBounds.min.y, 0.5 * transformedBounds.min.z + 0.5 * transformedBounds.max.z);
		Vector r = axis.normalize();
		float minAxisValue = std::min(std::min(fabs(r.x), fabs(r.y)), fabs(r.z));
		Vector s = minAxisValue == fabs(r.x) ? Vector(0, -r.z, r.y) : minAxisValue == fabs(r.y) ? Vector(-r.z, 0, r.x) : Vector(-r.y, r.x, 0);
		s = s.normalize();
		Vector t = cross(r, s).normalize();

		Matrix M = Matrix::system(r, s, t);
		Matrix rotX = Matrix::identity();
		
		rotX[1][1] = cos(angle);
		rotX[1][2] = -sin(angle);
		rotX[2][1] = sin(angle);
		rotX[2][2] = cos(angle);

		translate(-translateCenter);
		m_transformation = product(M.transpose(), m_transformation);
		m_transformation = product(rotX, m_transformation);
		m_transformation = product(M, m_transformation);
		translate(translateCenter);
	}

	BBox Instance::getBounds() const {
		BBox bounds = m_content->getBounds();
		Point transformedMax = m_transformation * bounds.max;
		Point transformedMin = m_transformation * bounds.min;
		bounds.max = max(transformedMax, transformedMin);
		bounds.min = min(transformedMin, transformedMax);

		return bounds;
	}

	void Instance::setCoordMapper(CoordMapper* coordMapper) {
		NOT_IMPLEMENTED;
	}

	void Instance::setMaterial(Material* material) {
		NOT_IMPLEMENTED;
	}
}