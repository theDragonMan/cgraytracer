#ifndef CG1RAYTRACER_PRIMMOD_NMAP_HEADER
#define CG1RAYTRACER_PRIMMOD_NMAP_HEADER

#include <vector>
#include <rt/primitive.h>
#include <core/point.h>

namespace rt {

class Triangle;
class Texture;

class NormalMapper : public Primitive {
	Point m_bv1;
	Point m_bv2;
	Point m_bv3;
	float m_vscale;

	Texture* m_normalmap;
	Triangle* m_base;
public:
    NormalMapper(Triangle* base, Texture* normalmap, const Point& bv1, const Point& bv2, const Point& bv3);
    virtual BBox getBounds() const;
    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
    virtual void setMaterial(Material* m);
    virtual void setCoordMapper(CoordMapper* cm);
	virtual bool intersectWithBox(const BBox& bbox) const;
};

}

#endif