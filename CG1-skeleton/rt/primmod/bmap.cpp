#include "core/matrix.h"
#include "rt/solids/triangle.h"
#include "rt/textures/imagetex.h"
#include "rt/bbox.h"
#include "rt/intersection.h"
#include "bmap.h"

namespace rt {
	BumpMapper::BumpMapper(Triangle* base, Texture* bumpmap, const Point& bv1, const Point& bv2, const Point& bv3, float vscale)
	:m_base(base), m_bumpmap(bumpmap), m_bv1(bv1), m_bv2(bv2), m_bv3(bv3), m_vscale(vscale)
	{}

	BBox BumpMapper::getBounds() const {
		return m_base->getBounds();
	}

	Intersection BumpMapper::intersect(const Ray& ray, float previousBestDistance) const {
		Intersection is = m_base->intersect(ray, previousBestDistance);
		if (is) {
			Point barrCoords = is.local();
			Point texCoord(barrCoords.x * m_bv1.x + barrCoords.y * m_bv2.x + barrCoords.z * m_bv3.x, barrCoords.x * m_bv1.y + barrCoords.y * m_bv2.y + barrCoords.z * m_bv3.y, barrCoords.x * m_bv1.z + barrCoords.y * m_bv2.z + barrCoords.z * m_bv3.z);
			float bumpX = m_vscale * m_bumpmap->getColorDX(texCoord).r;
			float bumpY = m_vscale * m_bumpmap->getColorDY(texCoord).r;

			Vector pU = m_base->p2 - m_base->p1;
			Vector pV = m_base->p3 - m_base->p1;			
			
			Vector tU = m_bv2 - m_bv1;
			Vector tV = m_bv3 - m_bv1;

			float dcr = tU.x * tV.y - tU.y * tV.x;
			float px = tV.y / dcr;
			float qx = -tU.y / dcr;

			float py = -tV.x / dcr;
			float qy = tU.x / dcr;

			Vector dNdx = px * pU + qx * pV;
			Vector dNdy = py * pU + qy * pV;

			Vector bumpNormal = (is.m_normal - bumpX * dNdx - bumpY * dNdy).normalize();

			is.m_normal = bumpNormal;
		}

		return is;
	}

	bool BumpMapper::intersectWithBox(const BBox& bbox) const {
		return m_base->intersectWithBox(bbox);
	}

	void BumpMapper::setMaterial(Material* m) {
		m_base->setMaterial(m);
	}

	void BumpMapper::setCoordMapper(CoordMapper* texMapper) { 
		m_base->setCoordMapper(texMapper);
	}
}