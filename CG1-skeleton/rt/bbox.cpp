#include <cmath>
#include <algorithm>
#include "core/assert.h"
#include "core/vector.h"
#include "core/point.h"
#include "rt/ray.h"
#include "rt/primitive.h"

#include "rt/bbox.h"

namespace rt {
	BBox BBox::empty() {
		return BBox(Point(FLT_MIN, FLT_MIN, FLT_MIN), Point(FLT_MIN, FLT_MIN, FLT_MIN));
	}

	BBox BBox::full() {
		return BBox(Point(FLT_MIN, FLT_MIN, FLT_MIN), Point(FLT_MAX, FLT_MAX, FLT_MAX));
	}

	Vector BBox::diagonal() const {
		Vector d;
		if (min.x != FLT_MIN && max.x != FLT_MAX) {
			d.x = max.x - min.x;
		} else {
			d.x = FLT_MAX;
		}

		if (min.y != FLT_MIN && max.y != FLT_MAX) {
			d.y = max.y - min.y;
		} else {
			d.y = FLT_MAX;
		}

		if (min.z != FLT_MIN && max.z != FLT_MAX) {
			d.z = max.z - min.z;
		} else {
			d.z = FLT_MAX;
		}

		return d;
	}

	void BBox::extend(const Point& p) {
		if (min == max && min.x == FLT_MIN) {
			min = p;
			max = p;
		}

		min = rt::min(p, min);
		max = rt::max(p, max);
	}

	void BBox::extend(const BBox& box) {
		if (min == max && min.x == FLT_MIN) {
			min = box.min;
			max = box.max;
		}

		min = rt::min(box.min, min);
		max = rt::max(box.max, max);
	}

	bool BBox::isUnbound() {
		if(min.x == max.x && min.x == FLT_MIN) return false;

		return min.x == FLT_MIN || max.x == FLT_MAX || 
			   min.y == FLT_MIN || max.y == FLT_MAX ||
			   min.z == FLT_MIN || max.z == FLT_MAX;
	}

	//take into account if the box is unbounded
	std::pair<float, float> BBox::intersect(const Ray& ray) const {
		if (min == max) {
			return std::pair<float, float>(0.0f, -1.0f);
		}

		float tminX = min.x != FLT_MIN ? (min.x - ray.o.x) / ray.d.x : ray.d.x > 0 ? FLT_MIN : FLT_MAX;
		float tmaxX = max.x != FLT_MAX ? (max.x - ray.o.x) / ray.d.x : ray.d.x < 0 ? FLT_MIN : FLT_MAX;

		if (tminX > tmaxX) {
			std::swap(tminX, tmaxX);
		}

		float tminY = min.y != FLT_MIN ? (min.y - ray.o.y) / ray.d.y : ray.d.y > 0 ? FLT_MIN : FLT_MAX; 
		float tmaxY = max.y != FLT_MAX ? (max.y - ray.o.y) / ray.d.y : ray.d.y < 0 ? FLT_MIN : FLT_MAX;

		if (tminY > tmaxY) std::swap(tminY, tmaxY);

		if (tminX > tmaxY || tmaxX < tminY) {
			return std::pair<float, float>(0.0f, -1.0f);
		}

		float tmin = std::max(tminY, tminX);
		float tmax = std::min(tmaxY, tmaxX);

		float tminZ = min.z != FLT_MIN ? (min.z - ray.o.z) / ray.d.z : ray.d.z > 0 ? FLT_MIN : FLT_MAX;
		float tmaxZ = max.z != FLT_MAX ? (max.z - ray.o.z) / ray.d.z : ray.d.z < 0 ? FLT_MIN : FLT_MAX;

		if (tminZ > tmaxZ) std::swap(tminZ, tmaxZ);

		if (tmin > tmaxZ || tmax < tminZ) {
			return std::pair<float, float>(0.0f, -1.0f);
		}

		tmin = tmin < tminZ ? tminZ : tmin;
		tmax = tmax > tmaxZ ? tmaxZ : tmax;
		
		return std::pair<float, float>(tmin, tmax);
	}

	float BBox::getArea() const {
		float a = max.x - min.x;
		float b = max.y - min.y;
		float c = max.z - min.z;

		return 2*a*b + 2*b*c + 2*a*c;
	}

	std::vector<Ray> BBox::boxRays() const {
		std::vector<Ray> rays;
		
		Vector r1Dir = Vector(1, 0, 0);
		Vector r2Dir = Vector(0, 1, 0);
		Vector r3Dir = Vector(0, 0, 1);
		
		rays.push_back(Ray(min, r1Dir));
		rays.push_back(Ray(min, r2Dir));
		rays.push_back(Ray(min, r3Dir));

		rays.push_back(Ray(Point(min.x,max.y,min.z), r1Dir));
		rays.push_back(Ray(Point(min.x, min.y, max.z), r2Dir));
		rays.push_back(Ray(Point(min.x, max.y, min.z), r3Dir));

		rays.push_back(Ray(Point(min.x, max.y, max.z), r1Dir));
		rays.push_back(Ray(Point(min.x, min.y, max.z), r1Dir));
		rays.push_back(Ray(Point(max.x, max.y, min.z), r3Dir));

		rays.push_back(Ray(Point(max.x, min.y, min.z), r3Dir));
		rays.push_back(Ray(Point(max.x, min.y, min.z), r2Dir));
		rays.push_back(Ray(Point(max.x, min.y, max.z), r2Dir));

		return rays;
	}

	bool BBox::isPointInside(const Point& p) const {
		return p.x >= min.x && p.y >= min.y && p.z >= min.z
			&& p.x <= max.x && p.y <= max.y && p.z <= max.z;
	}
}