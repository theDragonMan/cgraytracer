#ifndef CG1RAYTRACER_RENDERER_HEADER
#define CG1RAYTRACER_RENDERER_HEADER

#include <core/scalar.h>
#include <core/color.h>
#include <SDL.h>

#define RENDERER_MULTITHREADING

namespace rt {

class Image;
class Camera;
class Integrator;

class Renderer {
public:
	static SDL_Window*		sdl_window;
	static SDL_Surface*		sdl_surface;
	static SDL_Renderer*	sdl_renderer;
	static bool				isRunning;
public:
    Renderer(Camera* cam, Integrator* integrator);
    void setSamples(uint samples);
    void render(Image& img);
    void test_render1(Image& img);
    void test_render2(Image& img);
	void ChangePixelColor(uint x, uint y, const RGBColor& color) const;
private:
	
	RGBColor ComputeColor(float x, float y) const;

private:
    Camera*			cam;
    Integrator*		integrator;
    uint			samples;
};

}

#endif
