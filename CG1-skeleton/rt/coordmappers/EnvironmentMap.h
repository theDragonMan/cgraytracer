#ifndef ENVIRONMENT_MAP_H_
#define ENVIRONMENT_MAP_H_

#include "rt/intersection.h"
#include "coordmapper.h"

namespace rt {
	class EnvironmentMapper : public CoordMapper {
	public:
		EnvironmentMapper() {}
		virtual Point getCoords(const Intersection& hit) const;
	};

}

#endif // ! 
