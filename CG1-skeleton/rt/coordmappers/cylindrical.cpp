#include "core/matrix.h"
#include "rt/intersection.h"
#include "cylindrical.h"

namespace rt {
	CylindricalCoordMapper::CylindricalCoordMapper(const Point& origin, const Vector& longitudinalAxis, const Vector& polarAxis) 
	:m_origin(origin), m_longitudinalAxis(longitudinalAxis), m_polarAxis(polarAxis)
	{}

	Point CylindricalCoordMapper::getCoords(const Intersection& hit) const {
	
		Point hitPoint = hit.hitPoint();
		hitPoint = hitPoint + (Point::rep(0) - m_origin);

		Vector y = cross(m_longitudinalAxis, m_polarAxis).normalize();
		Vector x = cross(y, m_longitudinalAxis).normalize();
		Matrix invSystemMatrix = Matrix::system(x, y, m_longitudinalAxis.normalize()).invert();
		Point coords = invSystemMatrix * hitPoint;
		
		float cosineX = dot(Vector::right, Vector(coords.x, coords.y, 0).normalize());
		float phi = acos(cosineX);
		if (coords.y < 0) {
			phi = pi - phi;
		}

		float u = phi / (2 * pi) / m_polarAxis.length();

		float v = coords.z / m_longitudinalAxis.length();
		
		return Point(u, v, 0);
	}
}