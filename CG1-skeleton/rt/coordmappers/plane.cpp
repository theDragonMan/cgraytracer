#include "core/point.h"
#include "core/matrix.h"
#include "rt/intersection.h"
#include "rt/solids/solid.h"
#include "rt/bbox.h"
#include "plane.h"

namespace rt {
	PlaneCoordMapper::PlaneCoordMapper(const Vector& e1, const Vector& e2)
	:m_e1(e1), m_e2(e2)
	{}

	Point PlaneCoordMapper::getCoords(const Intersection& hit) const {
		Vector e3 = cross(m_e1, m_e2).normalize();
		Matrix invSystemMatrix = Matrix::system(m_e1, m_e2, e3).invert();
		Point localCartesianPoint = hit.local();
		Point coords = invSystemMatrix * localCartesianPoint;

		return Point(coords.x, coords.y, coords.z);
	}
}