#include <algorithm>

#include "core/point.h"
#include "EnvironmentMap.h"

namespace rt {
	Point EnvironmentMapper::getCoords(const Intersection& hit) const {
		int side = -1;
		float dirX = hit.ray.d.x;
		float dirY = hit.ray.d.y;
		float dirZ = hit.ray.d.z;
		float u, v;

		float maxCoord = std::max(std::max(abs(dirX), abs(dirY)), abs(dirZ));
		Vector projectedDir = hit.ray.d / maxCoord;
		if (abs(projectedDir.x) == 1) {
			side = (1 - (int)projectedDir.x) / 2;
			u = (-projectedDir.x * projectedDir.z + 1) / 2;
			v = (-projectedDir.y + 1) * 0.5f;
		} else if (abs(projectedDir.y) == 1) {
			side = projectedDir.y > 0 ? 2 : 3;
			u = (projectedDir.x + 1) * 0.5;
			v = (projectedDir.y * projectedDir.z + 1) * 0.5f;
		} else {
			side = projectedDir.z > 0 ? 4 : 5;
			u = (projectedDir.x + 1) * 0.5;
			v = (-projectedDir.z * projectedDir.y + 1) * 0.5f;
		}

		return Point(u, v, side);
	}
}