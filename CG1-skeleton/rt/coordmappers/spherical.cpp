#include <cmath>
#include "core/matrix.h"
#include "rt/intersection.h"
#include "spherical.h"

namespace rt {
	SphericalCoordMapper::SphericalCoordMapper()
	: m_origin(0, 0, 0), m_zenith(Vector::up), m_azimuth(Vector::right)
	{}

	SphericalCoordMapper::SphericalCoordMapper(const Point& origin, const Vector& zenith, const Vector& azimuthRef)
	: m_origin(origin), m_zenith(zenith), m_azimuth(azimuthRef)
	{}

	Point SphericalCoordMapper::getCoords(const Intersection& hit) const {
	//	Point hitPoint = hit.local();
		Point hitPoint = hit.hitPoint();
		hitPoint = hitPoint + (Point::rep(0) - m_origin);
		Vector y = cross(m_zenith, m_azimuth).normalize();
		Vector x = cross(y, m_zenith).normalize();
		Matrix invSystemMatrix(Float4(x), Float4(y), Float4(m_zenith.normalize()), Float4(0, 0, 0, 1));
		Point coord = invSystemMatrix * hitPoint;

		Vector h(coord.x, coord.y, coord.z);
		float theta = acos(dot(-Vector::forward, h.normalize()));
		Vector xzVector(coord.x, coord.y, 0);

		float phi = atan(coord.y / coord.x);
		if (xzVector.y < 0) {
			phi = pi - phi;
		}

		float u = phi / (2 * pi) / m_azimuth.length();
		float v = theta / pi / m_zenith.length();

		return Point(u, v, h.length());
	}
}