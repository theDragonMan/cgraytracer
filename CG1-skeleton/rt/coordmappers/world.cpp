#include "world.h"

namespace rt {
	WorldMapper::WorldMapper()
		:m_scaleFloat(Float4::rep(1.0f))
	{}

	WorldMapper::WorldMapper(const Float4& scale)
	:m_scaleFloat(scale)
	{}

	Point WorldMapper::getCoords(const Intersection& hit) const {
		Float4 hitPoint(hit.hitPoint());
		return Point(hitPoint.x * m_scaleFloat.x, hitPoint.y * m_scaleFloat.y, hitPoint.z * m_scaleFloat.z);
	}
}