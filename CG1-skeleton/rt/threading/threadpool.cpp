#include <SDL.h>
#include "core/image.h"
#include "threadpool.h"

namespace rt {
	RenderingThreadPool::RenderingThreadPool()
	: isPoolFinished(false), m_threadCount(0), m_imageHeight(0), m_imageWidth(0), m_tileHeight(0), m_tileWidth(0)
	{
		m_maxPossibleThreads = std::thread::hardware_concurrency();
	}

	RenderingThreadPool::RenderingThreadPool(int imgWidth, int imgHeight, int tileWidth, int tileHeight, int maxNumThreads)
	: isPoolFinished(false), m_threadCount(0), m_imageWidth(imgWidth), m_imageHeight(imgHeight), m_tileWidth(tileWidth), m_tileHeight(tileHeight), m_maxPossibleThreads(maxNumThreads)
	{
		uint hardwareThreads = std::thread::hardware_concurrency();
		if (hardwareThreads < maxNumThreads) {
			m_maxPossibleThreads = hardwareThreads;
		}
	}

	RenderingThreadPool::~RenderingThreadPool() {
		killAllThreads();
	}



	void RenderingThreadPool::addRenderingTask(int x, int y) {
		m_taskQueue.push(std::pair<int, int>(x, y));
	}


	std::mutex& RenderingThreadPool::getPoolMutex() {
		return m_syncMutex;
	}

	void RenderingThreadPool::start(std::function<void(int, int, Image&, ThreadObj&)>& renderFunction, Image& img) {

		auto threadFunction = [this, renderFunction](std::reference_wrapper<Image> imgRef, std::reference_wrapper<ThreadObj> threadObj) {
			threadObj.get().m_state = THREAD_RUNNING;
			Image& img = imgRef.get();
			this->m_threadCount++;
			while(threadObj.get().m_state == THREAD_RUNNING){
				std::pair<int, int> pixelCoords;
	
				this->m_queueMutex.lock();
				if(!this->m_taskQueue.empty()){
					pixelCoords = this->m_taskQueue.front();
					this->m_taskQueue.pop();
					this->m_queueMutex.unlock();
				} else {
					this->m_queueMutex.unlock();
					break;
				}
			
				renderFunction(pixelCoords.first, pixelCoords.second, img, threadObj.get());
			}
			this->m_threadCount--;
			threadObj.get().m_state = THREAD_DEAD;
			if (this->m_threadCount == 0) {
				isPoolFinished = true;
			}

		};

		for (int i = 0; i < m_maxPossibleThreads; i++) {
			ThreadObj* obj = new ThreadObj();
			obj->m_state = THREAD_START;
			obj->m_thread = new std::thread(threadFunction, std::ref(img), std::ref(*obj)) ;
			while(obj->m_state != THREAD_RUNNING) SDL_Delay(10);

			m_threads.push_back(obj);
		}
	}

	void RenderingThreadPool::killAllThreads() {
		for (auto t : m_threads) {
			if(t->m_state != THREAD_DEAD){
				t->m_state = THREAD_DYING;
				while(t->m_state != THREAD_DEAD) SDL_Delay(10);
			}

			t->m_thread->join();
			delete t->m_thread;
			delete t;
		}
		m_threads.clear();
	}

	uint RenderingThreadPool::getMaxPossibleThreads() {
		return m_maxPossibleThreads;
	}
}