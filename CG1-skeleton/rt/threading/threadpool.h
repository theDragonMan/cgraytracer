#ifndef RENDERING_THREAD_POOL_H_
#define RENDERING_THREAD_POOL_H_

#include <vector>
#include <thread>
#include <queue>
#include <mutex>

namespace rt {
	class Image;
	// this is a simple thread pool designed to work with the CG renderer 
	// so it does not follow some generic thread pool design
	class RenderingThreadPool {
	public:
		enum ThreadState {
			THREAD_START,
			THREAD_RUNNING,
			THREAD_SLEEPING,
			THREAD_DYING,
			THREAD_DEAD
		};

		struct ThreadObj {
			std::thread*	m_thread;
			ThreadState		m_state;
		};
	private:
		int m_imageWidth;
		int m_imageHeight;
		int m_tileWidth;
		int m_tileHeight;
		std::vector<ThreadObj*> m_threads;
		int m_maxPossibleThreads;
		std::mutex m_syncMutex;
		std::mutex m_queueMutex;
		std::queue<std::pair<int, int>> m_taskQueue;
		int m_threadCount;
	public:
		bool isPoolFinished;
	public:
		RenderingThreadPool();
		RenderingThreadPool(int imgWidth, int imgHeight, int tileWidth, int tileHeight, int maxNumThreads);
		~RenderingThreadPool();


		void addRenderingTask(int x, int y);
		void start(std::function<void(int, int, Image&, ThreadObj&)>& renderFunction, Image& img);
		void killAllThreads();
		
		std::mutex& getPoolMutex();
		uint getMaxPossibleThreads();
	};
	
}

#endif