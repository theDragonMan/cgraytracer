#include <iostream>
#include "core/assert.h"
#include "core/scalar.h"
#include "lambertian.h"

namespace rt {
	LambertianMaterial::LambertianMaterial(Texture* emission, Texture* diffuse)
	: m_emission(emission), m_diffuse(diffuse)
	{}

	RGBColor LambertianMaterial::getReflectance(const Point& textPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		//assert(m_diffuse) << "LambertianMaterial.cpp Line:" << __LINE__ << " Diffuse texture pointer is null";

		RGBColor reflectanceColor = m_diffuse->getColor(textPoint) / pi;
		return reflectanceColor;
	}

	RGBColor LambertianMaterial::getEmission(const Point& textPoint, const Vector& normal, const Vector& outDir) const {
		//assert(m_emission) << "LambertianMaterial.cpp Line:" << __LINE__ << " Emission texture pointer is null";
		return m_emission->getColor(textPoint);
	}

	Material::SampleReflectance LambertianMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return SampleReflectance(Vector(0, 0, 0), RGBColor(0, 0, 0));
	}

	Material::Sampling LambertianMaterial::useSampling() const {
		return SAMPLING_NOT_NEEDED;
	}
}