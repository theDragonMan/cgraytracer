#include <algorithm>
#include <math.h>
#include "core/assert.h"
#include "phong.h"

namespace rt {
	PhongMaterial::PhongMaterial(Texture* specular, float exponent) 
	:m_specular(specular), m_exponent(exponent)
	{}

	RGBColor PhongMaterial::getReflectance(const Point& textCoord, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		Vector reflectedLightDir = Vector::reflectVector(normal, inDir);
		reflectedLightDir = reflectedLightDir.normalize();

		float cosRefl = std::max(0.0f, dot(reflectedLightDir, outDir));

		float k = pow(cosRefl, (m_exponent + 2) /(2 * pi));
	
		float cosSurface = dot(normal, inDir);
		if (cosSurface < 9e-2f) {
			return RGBColor(0, 0, 0);
		}
		return m_specular->getColor(textCoord) * k;
	}

	RGBColor PhongMaterial::getEmission(const Point& textCoord, const Vector& normal, const Vector& outDir) const {
		return RGBColor(0, 0, 0);
	}

	Material::SampleReflectance PhongMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return SampleReflectance(Vector(0, 0, 0), RGBColor(0, 0, 0));
	}

	Material::Sampling PhongMaterial::useSampling() const {
		return SAMPLING_NOT_NEEDED;
	}
}