#include <cmath>
#include "core/random.h"
#include "glass.h"

namespace rt {
	GlassMaterial::GlassMaterial(float eta) 
	:m_eta(eta)
	{}

	RGBColor GlassMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		return RGBColor::rep(0.0f);
	}

	RGBColor GlassMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return RGBColor(0, 0, 0);
	}

	Material::SampleReflectance GlassMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		float sampleDir = random(0, 1.0f);
	
		float eta1 = 1;
		float eta2 = m_eta;

		float cosI = dot(outDir, normal);
		Vector n = normal;

		float n1 = cosI < 0 ? m_eta : 1.0f;
		float n2 = cosI < 0 ? 1.0f : m_eta;
		
		float ior = 1.0f / m_eta;
		if (cosI < 0) {
			cosI *= -1;
			n = -n;
			ior = m_eta;
		}

		Vector reflectedDir = Vector::reflectVector(n, outDir).normalize();
		float k = 1.0f - ior * ior * (1.0f - cosI * cosI);
		if (k < 0.0f) {
			return SampleReflectance(reflectedDir, RGBColor::rep(1));
		}

		Vector refractedDir = ior * -outDir + (ior * cosI - sqrt(k)) * n;
		float cosT = dot(-n, refractedDir.normalize());

		float fresnelParallel = (n2 * cosI - n1 * cosT) / (n2 * cosI + n1 * cosT);
		float fresnelOrtho = (n1 * cosI - n2 * cosT) / (n1 * cosI + n2 * cosT);
		float fresnel = (fresnelParallel * fresnelParallel + fresnelOrtho * fresnelOrtho) / 2;
		float refl = sampleDir < 0.5f ? fresnel : 1.0f - fresnel;
		Vector d = sampleDir < 0.5f ? reflectedDir : refractedDir.normalize();

		SampleReflectance sr(d, RGBColor(2 * refl, 2 * refl, 2 * refl));
		return sr;
	}
}