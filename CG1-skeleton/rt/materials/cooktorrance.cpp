
#include <iostream>
#include <algorithm>
#include "core/assert.h"
#include "core/scalar.h"
#include "cooktorrance.h"

namespace rt {

	CookTorranceMaterial::CookTorranceMaterial(Texture* diffuse, Texture* specular, float roughness, float diffuseKoef, float specularKoef)
	:m_diffuseTexture(diffuse) , m_specularTexture(specular), m_shininess(roughness), m_specularKoef(specularKoef)
	{}

	RGBColor CookTorranceMaterial::getReflectance(const Point& textPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		//assert(m_diffuse) << "LambertianMaterial.cpp Line:" << __LINE__ << " Diffuse texture pointer is null";
		RGBColor diffuseColor = m_diffuseKoef * m_diffuseTexture->getColor(textPoint);
		RGBColor specularColor = m_specularTexture->getColor(textPoint);

		Vector h = inDir + outDir;

		float f0 = (1 - m_refractionIndex) * (1 - m_refractionIndex) / (1 + m_refractionIndex);
		float viewHalfCoef = dot(outDir, h);
		float viewCoef = dot(outDir, normal);
		float lightCoef = dot(inDir, normal);
		float lightHalfCoef = dot(inDir, h);
		float halfNormalCoef = dot(h, normal);

		float fresnel = f0 + (1 - f0) * pow(1 - lightHalfCoef, 5);
		float d = pow(halfNormalCoef, m_shininess) * (m_shininess + 2) / (2 * pi); //bling-phong distribution

		float g1 = 2 * halfNormalCoef * viewCoef / viewHalfCoef;
		float g2 = 2 * halfNormalCoef * lightCoef / viewHalfCoef;
		float g = std::min(std::min(g1, g2), 1.0f);

		float ks = fresnel * d * g / (pi * viewCoef * lightCoef);
		RGBColor pointColor = diffuseColor + m_specularKoef * ks * specularColor;

		return pointColor;
	}

	RGBColor CookTorranceMaterial::getEmission(const Point& textPoint, const Vector& normal, const Vector& outDir) const {
		//assert(m_emission) << "LambertianMaterial.cpp Line:" << __LINE__ << " Emission texture pointer is null";
		return RGBColor(0, 0, 0);
	}

	Material::SampleReflectance CookTorranceMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return SampleReflectance(Vector(0, 0, 0), RGBColor(0, 0, 0));
	}

	Material::Sampling CookTorranceMaterial::useSampling() const {
		return SAMPLING_NOT_NEEDED;
	}
}