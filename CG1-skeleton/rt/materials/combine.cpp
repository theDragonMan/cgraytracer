#include "core/assert.h"
#include "combine.h"

namespace rt {
	CombineMaterial::CombineMaterial()
	:m_sampleMaterial(std::pair<Material*, float>(nullptr, 0))
	{}

	void CombineMaterial::add(Material* text, float weight) {
		if (text->useSampling() == SAMPLING_ALL) {
			m_sampleMaterial = std::pair<Material*, float>(text, weight);
		} else {
			m_materials.push_back(std::pair<Material*, float>(text, weight));
		}

	}

	RGBColor CombineMaterial::getReflectance(const Point& textCoord, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		RGBColor combinedReflectance(0, 0, 0);

		for (auto& material : m_materials) {
			RGBColor materialRefl = material.first->getReflectance(textCoord, normal, outDir, inDir) * material.second;
			combinedReflectance = combinedReflectance + materialRefl;
		}

		return combinedReflectance;
	}

	RGBColor CombineMaterial::getEmission(const Point& textCoord, const Vector& normal, const Vector& outDir) const {
		RGBColor combinedEmission(0, 0, 0);
		for (auto& material : m_materials) {
			RGBColor materailEmission = material.first->getEmission(textCoord, normal, outDir) * material.second;
			combinedEmission = combinedEmission + materailEmission;
		}

		return combinedEmission;
	}

	Material::SampleReflectance CombineMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		if (m_sampleMaterial.first) {
			SampleReflectance sr = m_sampleMaterial.first->getSampleReflectance(texPoint, normal, outDir);
			sr.reflectance = sr.reflectance * m_sampleMaterial.second;
			return sr;
		}

		return SampleReflectance(Vector(0, 0, 0), RGBColor(0, 0, 0));
	}

	Material::Sampling CombineMaterial::useSampling() const {
		
		return m_sampleMaterial.first && m_materials.size() ? SAMPLING_SECONDARY :
			   m_sampleMaterial.first ? SAMPLING_ALL : SAMPLING_NOT_NEEDED;
	}
}