#include <cmath>
#include <algorithm>
#include "core/assert.h"

#include "dummy.h"

namespace rt {
	DummyMaterial::DummyMaterial()
	{}

	RGBColor DummyMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return RGBColor(0, 0, 0);
	}

	RGBColor DummyMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		float reflectedLight = dot(normal, inDir);
		return RGBColor(reflectedLight, reflectedLight, reflectedLight);
	}

	DummyMaterial::SampleReflectance DummyMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		NOT_IMPLEMENTED;
	}
}