#include <iostream>
#include "core/assert.h"
#include "core/scalar.h"
#include "mirror.h"

namespace rt {

	MirrorMaterial::MirrorMaterial(float eta, float kappa)
	: m_eta(eta), m_kappa(kappa)
	{}

	RGBColor MirrorMaterial::getReflectance(const Point& textPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		//assert(m_diffuse) << "LambertianMaterial.cpp Line:" << __LINE__ << " Diffuse texture pointer is null";

		return RGBColor(0, 0, 0);
	}

	RGBColor MirrorMaterial::getEmission(const Point& textPoint, const Vector& normal, const Vector& outDir) const {
		//assert(m_emission) << "LambertianMaterial.cpp Line:" << __LINE__ << " Emission texture pointer is null";
		return RGBColor(0, 0, 0);
	}

	Material::SampleReflectance MirrorMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		Vector reflectedOut = Vector::reflectVector(normal, outDir);
		reflectedOut = reflectedOut.normalize();
		float cosDir = dot(normal, outDir);
		float r1 = ((m_eta * m_eta + m_kappa *m_kappa) * cosDir * cosDir - 2 * m_eta * cosDir + 1) / ((m_eta * m_eta + m_kappa *m_kappa) * cosDir * cosDir + 2 * m_eta * cosDir + 1);
		float r2 = ((m_eta * m_eta + m_kappa *m_kappa) - 2 * m_eta * cosDir + cosDir * cosDir) / ((m_eta * m_eta + m_kappa *m_kappa) + 2 * m_eta * cosDir + cosDir * cosDir);
		float refl = (r1 * r1 + r2 * r2) / 2;
		return SampleReflectance(reflectedOut, RGBColor(refl, refl, refl));
	}

	Material::Sampling MirrorMaterial::useSampling() const {
		return SAMPLING_ALL;
	}
}