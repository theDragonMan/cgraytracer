#include <cmath>
#include "core/scalar.h"
#include "core/random.h"
#include "fuzzymirror.h"

namespace rt {
	FuzzyMirrorMaterial::FuzzyMirrorMaterial(float eta, float kappa, float fuzzyangle)
	: m_eta(eta), m_kappa(kappa), m_fuzzyAngle(fuzzyangle)
	{}

	RGBColor FuzzyMirrorMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		return RGBColor::rep(0.0f);
	}

	RGBColor FuzzyMirrorMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return RGBColor(0, 0, 0);
	}

	Material::SampleReflectance FuzzyMirrorMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		Vector reflectedOut = Vector::reflectVector(normal, outDir).normalize();
		float cosDir = dot(normal, outDir);
		float r1 = ((m_eta * m_eta + m_kappa *m_kappa) * cosDir * cosDir - 2 * m_eta * cosDir + 1) / ((m_eta * m_eta + m_kappa *m_kappa) * cosDir * cosDir + 2 * m_eta * cosDir + 1);
		float r2 = ((m_eta * m_eta + m_kappa *m_kappa) - 2 * m_eta * cosDir + cosDir * cosDir) / ((m_eta * m_eta + m_kappa *m_kappa) + 2 * m_eta * cosDir + cosDir * cosDir);
		float refl = (r1 * r1 + r2 * r2) / 2;

		Vector xAxis = cross(reflectedOut, normal).normalize();
		Vector yAxis = cross(xAxis, reflectedOut).normalize();

		float u = random(0, 1);
		float v = sqrt(random(0, 1));
	
		float radius = v * tan(m_fuzzyAngle);
		float polarAngle = u * 2 * pi;

		float x = radius * cos(polarAngle);
		float y = radius * sin(polarAngle);

		Vector perturbedReflection = x * xAxis + y * yAxis + reflectedOut;
		if (dot(normal, perturbedReflection) < 0) {
			return SampleReflectance(Vector(0, 0, 0), RGBColor(0, 0, 0));
		}
		return SampleReflectance(perturbedReflection.normalize(), RGBColor::rep(refl));
	}
}