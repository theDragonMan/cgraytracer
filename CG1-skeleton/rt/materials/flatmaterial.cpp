
#include <iostream>
#include <algorithm>
#include "core/assert.h"
#include "core/scalar.h"
#include "flatmaterial.h"

namespace rt {

	FlatMaterial::FlatMaterial(Texture* texture)
		:texture(texture)
	{}

	RGBColor FlatMaterial::getReflectance(const Point& textPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const {
		return RGBColor(0, 0, 0);
	}

	RGBColor FlatMaterial::getEmission(const Point& textPoint, const Vector& normal, const Vector& outDir) const {
		return texture->getColor(textPoint);
	}

	Material::SampleReflectance FlatMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const {
		return SampleReflectance(Vector(0, 0, 0), RGBColor(0, 0, 0));
	}
}