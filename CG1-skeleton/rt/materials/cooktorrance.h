#ifndef COOK_TORRANCE_H_
#define COOK_TORRANCE_H_

#include "rt/textures/texture.h"
#include "material.h"

namespace rt {
	class CookTorranceMaterial : public Material {
	private:
		Texture* m_diffuseTexture;
		Texture* m_specularTexture;
		
		float m_refractionIndex;
		float m_shininess;
		float m_specularKoef;
		float m_diffuseKoef;
	public:
		CookTorranceMaterial(Texture* diffuse, Texture* specular, float roughness, float diffuseCoef, float specularKoef);
		virtual RGBColor getReflectance(const Point& textCoord, const Vector& normal, const Vector& outDir, const Vector& inDir) const;
		virtual RGBColor getEmission(const Point& textCoord, const Vector& normal, const Vector& outDir) const;
		virtual SampleReflectance getSampleReflectance(const Point& textCoord, const Vector& normal, const Vector& outDir) const;
		virtual Sampling useSampling() const;
	};
}

#endif