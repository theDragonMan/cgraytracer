#include "rt/ray.h"
#include "rt/cameras/orthographic.h"

namespace rt {
	OrthographicCamera::OrthographicCamera(const Point& center, const Vector& forward, const Vector& up, float scaleX, float scaleY)
	: m_center(center), m_forward(forward), m_up(up), m_scaleX(scaleX), m_scaleY(scaleY)
	{}

	Ray OrthographicCamera::getPrimaryRay(float x, float y) const {
		Ray ray;
		
		Vector xAxis = cross(m_forward, m_up).normalize() * m_scaleX / 2;
		Vector yAxis = cross(xAxis, m_forward).normalize() * m_scaleY / 2;
		ray.o = m_center + m_forward + x * xAxis + y * yAxis;
		ray.d = m_forward;

		return ray;
	}
}