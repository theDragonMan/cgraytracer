#ifndef CG1RAYTRACER_CAMERAS_DOFPERSPECTIVE_HEADER
#define CG1RAYTRACER_CAMERAS_DOFPERSPECTIVE_HEADER

#include <rt/cameras/camera.h>
#include <core/vector.h>
#include <core/point.h>

namespace rt {

class DOFPerspectiveCamera : public Camera {
	Point m_center;
	Vector m_forward;
	Vector m_xAxis;
	Vector m_yAxis;
	float m_focalDistance;
	float m_apertureRadius;
public:
    DOFPerspectiveCamera(
        const Point& center,
        const Vector& forward,
        const Vector& up,
        float verticalOpeningAngle,
        float horizonalOpeningAngle,
        float focalDistance,
        float apertureRadius
        );

    virtual Ray getPrimaryRay(float x, float y) const;

};

}


#endif
