#include <cmath>
#include "rt/ray.h"

namespace rt {

	Ray::Ray(const Point& o, const Vector& d)
	: o(o), d(d.normalize())
	{
	}

	Point Ray::getPoint(float distance) const {
		Point p = o + distance * d;
		p.x = roundf(p.x * 1000) / 1000;
		p.y = roundf(p.y * 1000) / 1000;
		p.z = roundf(p.z * 1000) / 1000;

		return p;
	}
}