#include <cmath>

#include "rt/ray.h"
#include "rt/cameras/perspective.h"


namespace rt {
	PerspectiveCamera::PerspectiveCamera(const Point& center, const Vector& forward, const Vector& up, float verticalOpeningAngle, float horizonalOpeningAngle)
	: m_center(center), m_forward(forward.normalize()), m_up(up.normalize()), m_verticalOpeningAngle(verticalOpeningAngle), m_horizontalOpeningAngle(horizonalOpeningAngle)
	{
		m_xAxis = cross(m_forward, m_up).normalize() * tan(m_horizontalOpeningAngle * 0.5f);
		m_yAxis = cross(m_xAxis, m_forward).normalize() * tan(m_verticalOpeningAngle * 0.5f);
	}

	Ray PerspectiveCamera::getPrimaryRay(float x, float y) const {
		Ray ray;
		ray.o = m_center;
		ray.d = m_forward + x * m_xAxis + y * m_yAxis;
		ray.d = ray.d.normalize();

		return ray;
	}
}
