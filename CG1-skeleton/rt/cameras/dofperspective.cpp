#include <cmath>
#include "core/random.h"
#include "core/scalar.h"
#include "rt/ray.h"
#include "dofperspective.h"

namespace rt {
	DOFPerspectiveCamera::DOFPerspectiveCamera(const Point& center,
		const Vector& forward,
		const Vector& up,
		float verticalOpeningAngle,
		float horizonalOpeningAngle,
		float focalDistance,
		float apertureRadius)
		:m_center(center), m_forward(forward.normalize()), m_focalDistance(focalDistance), m_apertureRadius(apertureRadius)
		{
			m_xAxis = cross(m_forward, up).normalize() * tan(horizonalOpeningAngle * 0.5f);
			m_yAxis = cross(m_xAxis, m_forward).normalize() * tan(verticalOpeningAngle * 0.5f);
		}

	Ray DOFPerspectiveCamera::getPrimaryRay(float x, float y) const {
		Vector d = m_forward * m_focalDistance + x * m_xAxis + y * m_yAxis;
		Point h = m_center + d;
		float k1 = random(0, 1.0f);
		float k2 = sqrt(random(0, 1.0f));

		float newX = k2 * m_apertureRadius * cos(2 * pi * k1);
		float newY = k2 * m_apertureRadius * sin(2 * pi * k1);

		Point newCenter = m_center + newX * m_xAxis.normalize() + newY * m_yAxis.normalize();
		Vector newDir = (h - newCenter).normalize();
		return Ray(newCenter, newDir);
	}
}