#ifndef QUADRIC_H_
#define QUADRIC_H_

#include <vector>
#include "core/point.h"
#include "core/vector.h"

#include "rt/solids/solid.h"
#include "rt/bbox.h"

namespace rt {

class Quadric : public Solid {
	Point m_center;
	std::vector<float> m_parameters;

public:

	Quadric(){}
	Quadric(const Point& center, const std::vector<float>& params);

	virtual BBox getBounds() const;
	virtual Intersection intersect(const Ray& ray, float previousBestDistance = FLT_MAX) const;
	virtual Point sample() const;
	virtual float getArea() const;
	virtual bool intersectWithBox(const BBox& box) const;
};

}

#endif

