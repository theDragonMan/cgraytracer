#include "core/vector.h"
#include "core/assert.h"
#include "core/point.h"
#include "core/color.h"
#include "rt/world.h"
#include "rt/intersection.h"

#include "rt/integrators/casting.h"


namespace rt {

	RGBColor RayCastingIntegrator::getRadiance(const Ray& ray) const {
		Intersection is = world->scene->intersect(ray);
		if (is) {
			
			float k = dot(-ray.d, is.normal()); //normalizing the dot product. we need values in [0 , 1]
			return RGBColor(k, k, k);
		}

		return RGBColor(0, 0, 0); // return black if we don't hit anything
	}
}