#ifndef INFINITE_BOX_H_
#define INFINITE_BOX_H_

#include "rt/coordmappers/EnvironmentMap.h"
#include "rt/solids/solid.h"

namespace rt {
	class InfiniteBox : public Solid {
	public:
		InfiniteBox(){}
		InfiniteBox(CoordMapper* texMapper, Material* material);

		virtual Intersection intersect(const Ray& ray, float previousBestDistance = FLT_MAX) const;
		virtual Point sample() const;
		virtual float getArea() const;
		virtual BBox getBounds() const;
		virtual bool intersectWithBox(const BBox& bbox) const;
	};
}

#endif // ! INFINITE_BOX_H_
