#include <cmath>
#include <algorithm>
#include "core/vector.h"
#include "core/assert.h"
#include "rt/bbox.h"
#include "rt/intersection.h"

#include "rt/solids/sphere.h"


namespace rt {
	Sphere::Sphere(const Point& center, float radius, CoordMapper* texMapper, Material* material) 
	:Solid(texMapper, material), m_center(center), m_radius(radius)
	{}

	Intersection Sphere::intersect(const Ray& ray, float previousBestDistance) const {
		Vector CO = ray.o - m_center;
		float bHalf = dot(ray.d, CO);
		float c = CO.lensqr() - m_radius * m_radius;
		float D = bHalf * bHalf - c; // a = 1 because ray.d^2 == 1

		if (D < 0) {
			return Intersection::failure();
		}

		float distance;
		if(D == 0) {
			if (bHalf < 0) {
				return Intersection::failure();
			}

			distance = -bHalf;
			Vector normal = (ray.getPoint(distance) - m_center).normalize();
			return Intersection(distance, ray, this, normal, Point(0, 0, 0));
		}

		float t1 = -bHalf + sqrt(D);
		float t2 = -bHalf - sqrt(D);

		if (t1 < 0) {
			return Intersection::failure();
		}

		distance = t1 > 0 && t2 > 0 ? t2 : t1;
		if (distance > previousBestDistance) {
			return Intersection::failure();
		}

		Vector normal = (ray.getPoint(distance) - m_center).normalize();
		Point cartesianHitPoint = ray.getPoint(distance);

		return Intersection(distance, ray, this, normal, cartesianHitPoint);
	}

	float Sphere::getArea() const {
		return 4 * pi * m_radius * m_radius;
	}

	BBox Sphere::getBounds() const {
		Vector d(1, 1, 1);
		Point minP = m_center - m_radius * d;
		Point maxP = m_center + m_radius * d;

		return BBox(minP, maxP);
	}

	Point Sphere::sample() const {
		NOT_IMPLEMENTED;
	}

	bool Sphere::intersectWithBox(const BBox& box) const {
		float s, d = 0;
		for (int i = 0; i < 3; i++) {
			switch (i) {
				case 0: 
					if (m_center.x < box.min.x) {
						s = m_center.x - box.min.x;
						d += s*s;
					} else if (m_center.x > box.max.x) {
						s = m_center.x - box.max.x;
						d += s*s;
					}
					break;
				case 1:
					if (m_center.y < box.min.y) {
						s = m_center.y - box.min.y;
						d += s*s;
					} else if(m_center.y > box.max.y) {
						s = m_center.y - box.max.y;
						d += s*s;
					}
					break;
				case 2:
					if (m_center.z < box.min.z) {
						s = m_center.z - box.min.z;
						d += s*s;
					} else if(m_center.z > box.max.z) {
						s = m_center.z - box.max.z;
						d += s*s;
					}
					break;
			}
		}

		return d <= m_radius * m_radius;
	}

}