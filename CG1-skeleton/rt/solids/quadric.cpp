#include <cmath>
#include "core/assert.h"
#include "rt/intersection.h"
#include "quadric.h"

namespace rt {
	Quadric::Quadric(const Point& center, const std::vector<float>& params)
	:m_center(center), m_parameters(params)
	{}

	Intersection Quadric::intersect(const Ray& ray, float previousBestDistance) const {
		float xO = xO;
		float yO = yO;
		float zO = ray.o.z - m_center.z;


		float A = m_parameters[0] * ray.d.x * ray.d.x + m_parameters[1] * ray.d.y * ray.d.y +
				  m_parameters[2] * ray.d.z * ray.d.z + m_parameters[3] * ray.d.x * ray.d.y +
				  m_parameters[4] * ray.d.x * ray.d.z + m_parameters[5] * ray.d.y * ray.d.z;

		float B =	2 * (m_parameters[0] * xO * ray.d.x + m_parameters[1] * yO * ray.d.y + m_parameters[2] * zO * ray.d.z) +
					m_parameters[3] * (xO * ray.d.y + yO * ray.d.x) + m_parameters[4] * (xO * ray.d.z + yO * ray.d.x) + m_parameters[5] * (yO * ray.d.z + yO * ray.d.z) +
					m_parameters[6] * ray.d.x + m_parameters[7] * ray.d.y + m_parameters[8] * ray.d.z;

		float C = m_parameters[0] * xO * xO + m_parameters[1] * yO * yO + m_parameters[2] * zO * zO +
				  m_parameters[3] * xO * yO + m_parameters[4] * xO * zO + m_parameters[5] * yO * zO +
				  m_parameters[6] * xO + m_parameters[7] * yO + m_parameters[8] * zO + m_parameters[9];

		float t;
		if (A == 0) {
			float t = - C / B;
			if (t < 0.0f || t > previousBestDistance) {
				return Intersection::failure();
			}

		} else {
			float D = B * B - 4 * A * C;
			float t1 = (-B + sqrt(D)) / (2 * A);
			float t2 = (-B - sqrt(D)) / (2 * A);
			
			if (t1 < 0 && t2 < 0) {
				return Intersection::failure();
			}

			t = t1 > 0 && t2 > 0 ? t2 : t1;
		}

		Point hitPoint = ray.getPoint(t);

		float xNormal = 2 * m_parameters[0] * hitPoint.x + m_parameters[3] * hitPoint.y + m_parameters[4] * hitPoint.z + m_parameters[6];
		float yNormal = 2 * m_parameters[1] * hitPoint.y + m_parameters[3] * hitPoint.x + m_parameters[5] * hitPoint.z + m_parameters[7];
		float zNormal = 2 * m_parameters[2] * hitPoint.z + m_parameters[4] * hitPoint.x + m_parameters[5] * hitPoint.y + m_parameters[8];


		return Intersection(t, ray, this, Vector(xNormal, yNormal, zNormal), Point(0,0,0));
	}

	BBox Quadric::getBounds() const {
		NOT_IMPLEMENTED;
	}

	float Quadric::getArea() const {
		NOT_IMPLEMENTED;
	}

	bool Quadric::intersectWithBox(const BBox& box) const {
		NOT_IMPLEMENTED;
	}

	Point Quadric::sample() const {
		NOT_IMPLEMENTED;
	}
}