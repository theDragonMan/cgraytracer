
#include "rt/ray.h"
#include "rt/intersection.h"
#include "striangle.h"

namespace rt {
	SmoothTriangle::SmoothTriangle(Point vertices[3], Vector normals[3], CoordMapper* texMapper, Material* material)
	:Triangle(vertices, texMapper, material), n1(normals[0]), n2(normals[1]), n3(normals[2])
	{}


	SmoothTriangle::SmoothTriangle(const Point& v1, const Point& v2, const Point& v3,
		const Vector& n1, const Vector& n2, const Vector& n3,
		CoordMapper* texMapper, Material* material)
		:Triangle(v1,v2,v3, texMapper, material), n1(n1), n2(n2), n3(n3)
		{}

	Intersection SmoothTriangle::intersect(const Ray& ray, float previousBestDistance) const {
		Intersection is = Triangle::intersect(ray, previousBestDistance);
		if(is){
			Point barCoords = is.local();
			is.m_normal = (n1 * barCoords.x + n2 * barCoords.y + n3 * barCoords.z).normalize();
			
		}
		return is;
	}
}