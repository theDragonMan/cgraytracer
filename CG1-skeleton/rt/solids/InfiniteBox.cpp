#include "rt/bbox.h"
#include "InfiniteBox.h"

namespace rt {
	InfiniteBox::InfiniteBox(CoordMapper* texMapper, Material* material)
	:Solid(texMapper, material)
	{}

	Intersection InfiniteBox::intersect(const Ray& ray, float previousBestDistance) const {
		if (previousBestDistance < FLT_MAX) {
			return Intersection::failure();
		}

		return Intersection(FLT_MAX, ray, this, Vector::up, Point(0, 0, 0));
	}

	Point InfiniteBox::sample() const {
		return Point(0, 0, 0);
	}

	float InfiniteBox::getArea() const {
		return 0.0f;
	}

	bool InfiniteBox::intersectWithBox(const BBox& bbox) const {
		return true;
	}

	BBox InfiniteBox::getBounds() const {
		return BBox(Point(FLT_MIN, FLT_MIN, FLT_MIN), Point(FLT_MAX, FLT_MAX, FLT_MAX));
	}
}