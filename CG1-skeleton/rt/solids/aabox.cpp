#include <cmath>
#include "core/vector.h"
#include "core/assert.h"

#include "rt/intersection.h"
#include "rt/solids/aabox.h"

namespace rt {
	AABox::AABox(const Point& corner1, const Point& corner2, CoordMapper* texMapper, Material* material) {
		m_corner1 = min(corner1, corner2);
		m_corner2 = max(corner1, corner2);
	}


	Intersection AABox::intersect(const Ray& ray, float previousBestDistance) const {
		
		float tminX = (m_corner1.x - ray.o.x) / ray.d.x;
		float tmaxX = (m_corner2.x - ray.o.x) / ray.d.x;
		
		if (tminX > tmaxX) {
			std::swap(tminX, tmaxX);
		}

		float tminY = (m_corner1.y - ray.o.y) / ray.d.y;
		float tmaxY = (m_corner2.y - ray.o.y) / ray.d.y;

		if(tminY > tmaxY) std::swap(tminY, tmaxY);

		if (tminX > tmaxY || tmaxX < tminY) {
			return Intersection::failure();
		}

		float tmin = max(tminY, tminX);
		float tmax = min(tmaxY, tmaxX);

		float tminZ = (m_corner1.z - ray.o.z) / ray.d.z;
		float tmaxZ = (m_corner2.z - ray.o.z) / ray.d.z;

		if(tminZ > tmaxZ) std::swap(tminZ, tmaxZ);

		if (tmin > tmaxZ || tmax < tminZ) {
			return Intersection::failure();
		}

		tmin = tmin < tminZ ? tminZ : tmin;
		tmax = tmax > tmaxZ ? tmaxZ : tmax;
		
		if (tmin < 0 && tmax < 0) {
			return Intersection::failure();
		}
		
		float distance = tmin > 0 && tmax > 0 ? tmin : tmax; 
		
		if (distance < 0.0f) {
			return Intersection::failure();
		}

		if (distance > previousBestDistance) {
			return Intersection::failure();
		}

		Point hitPoint = ray.getPoint(distance);
		Vector normal;
		float maxX = max(m_corner1.x, m_corner2.x);
		float maxY = max(m_corner1.y, m_corner2.y);
		float maxZ = max(m_corner1.z, m_corner2.z);

		if(distance == tminX || distance == tmaxX) {
			normal = Vector(1, 0, 0) * (maxX == hitPoint.x ? 1 : -1);
		} else if(distance == tminY || distance == tmaxY) {
			normal = Vector(0, 1, 0) * (maxY == hitPoint.y ? 1 : -1);
		} else {
			normal = Vector(0, 0, 1) * (maxZ == hitPoint.z ? 1 : -1);
		}

		return Intersection(distance, ray, this, normal, Point(0, 0, 0));
	}

	float AABox::getArea() const {
		float sumArea = 0.0f;
		sumArea += 2 * abs(m_corner1.x - m_corner2.x) * abs(m_corner1.y - m_corner2.y);
		sumArea += 2 * abs(m_corner1.x - m_corner2.x) * abs(m_corner1.z - m_corner2.z);
		sumArea += 2 * abs(m_corner1.y - m_corner2.y) * abs(m_corner1.z - m_corner2.z);

		return sumArea;
	}

	BBox AABox::getBounds() const {
		return BBox(m_corner1, m_corner2);
	}

	bool AABox::intersectWithBox(const BBox& box) const {
		Point minNew = max(m_corner1, box.min);
		Point maxNew = min(m_corner2, box.max);
		
		return minNew.x <= maxNew.x && minNew.y <= maxNew.y && minNew.z <= maxNew.z;
	}

	Point AABox::sample() const {
		NOT_IMPLEMENTED;
	}
}

