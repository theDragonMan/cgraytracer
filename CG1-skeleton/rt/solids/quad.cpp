#include "core/vector.h"
#include "core/assert.h"
#include "core/random.h"
#include "rt/bbox.h"
#include "rt/intersection.h"
#include "rt/ray.h"

#include "quad.h"

namespace rt {
	Quad::Quad(const Point& v1, const Vector& span1, const Vector& span2, CoordMapper* texMapper, Material* material)
	:Solid(texMapper, material), m_v1(v1), m_span1(span1), m_span2(span2)
	{}

	Intersection Quad::intersect(const Ray& ray, float previousBestDistance) const {
		Vector planeNormal = cross(m_span1, m_span2).normalize();
		float denom = dot(planeNormal, ray.d);
		if (denom >= 0 && denom <= 1e-12) {
			return Intersection::failure();
		}
		Point p1 = m_v1 + m_span1;
		Point p2 = m_v1 + m_span2;

		float t = dot(m_v1 - ray.o, planeNormal) / denom;
		if (t < 0 || t >= previousBestDistance) {
			return Intersection::failure();
		}

		Point planePoint = ray.getPoint(t);

		planeNormal = denom > 0 ? -planeNormal : planeNormal;

	
		Vector planeVector = cross(m_span1, m_span2);
		float dcr = dot(planeVector, -ray.d);
		Vector h = ray.o - m_v1;

		double lambda2 = dot(cross(h, m_span2), -ray.d) / dcr;
		double lambda3 = dot(cross(m_span1, h), -ray.d) / dcr;
		

		if (lambda2 >= 0.0f && lambda2 <= 1.0f && lambda3 >= 0.0f && lambda3 <= 1.0f && lambda2 + lambda3 <= 1) {
			return Intersection(t, ray, this, planeNormal, Point(1 - lambda2 - lambda3, lambda2, lambda3));
		}

		Point p3 = m_v1 + m_span1 + m_span2;
		Vector h2 = ray.o - p3;

		lambda2 = dot(cross(h2, m_span1), -ray.d) / dcr;
		lambda3 = dot(cross(m_span2, h2), -ray.d) / dcr;

		if (lambda2 >= 0.0f && lambda2 <= 1.0f && lambda3 >= 0.0f && lambda3 <= 1.0f && lambda2 + lambda3 <= 1) {
			return Intersection(t, ray, this, planeNormal, Point(1 - lambda2 - lambda3, lambda2, lambda3));
		}

		return Intersection::failure();
	}

	float Quad::getArea() const {
		return cross(m_span1, m_span2).length();
	}

	Point Quad::sample() const {
		float u = random(0, 1);
		float v = random(0, 1);
		Point p2 = m_v1 + m_span1;
		Point p3 = m_v1 + m_span2;

		float newX = m_v1.x * (1 - u - v) + p2.x * u + p3.x * v;
		float newY = m_v1.y * (1 - u - v) + p2.y * u + p3.y * v;
		float newZ = m_v1.z * (1 - u - v) + p2.z * u + p3.z * v;

		return Point(newX, newY, newZ);
	}

	BBox Quad::getBounds() const {
		Point p2 = m_v1 + m_span1;
		Point p3 = m_v1 + m_span2;
		Point p4 = m_v1 + m_span1 + m_span2;

		Point minP = min(min(m_v1, p2), min(p3, p4));
		Point maxP = max(max(m_v1, p2), max(p3, p4));
		minP.x -= 1e-3;
		minP.y -= 1e-3;
		minP.z -= 1e-3;

		maxP.x -= 1e-3;
		maxP.y -= 1e-3;
		maxP.z -= 1e-3;

		return  BBox(minP, maxP);
	}

	bool Quad::intersectWithBox(const BBox& box) const {
		float distX = box.max.x - box.min.x;
		float distY = box.max.y - box.min.y;
		float distZ = box.max.z - box.min.z;

		std::vector<Ray> rays = box.boxRays();
		for (Ray& r : rays) {
			Intersection is = intersect(r);
			if (is) {
				if (r.d == Vector(1, 0, 0)) {
					return distX >= is.distance;
				}
				else if (r.d == Vector(0, 1, 0)) {
					return distY >= is.distance;
				}
				else {
					return distZ >= is.distance;
				}
			}
		}
	}
}