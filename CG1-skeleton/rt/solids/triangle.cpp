#include <cmath>
#include <algorithm>
#include "core/vector.h"
#include "core/assert.h"
#include "core/random.h"
#include "rt/bbox.h"
#include "rt/ray.h"
#include "rt/intersection.h"

#include "triangle.h"

namespace rt {
	Triangle::Triangle(Point vertecies[3], CoordMapper* texMapper, Material* material)
	: Solid(texMapper,material), p1(vertecies[0]), p2(vertecies[1]), p3(vertecies[2])
	{}

	Triangle::Triangle(const Point& v1, const Point& v2, const Point& v3, CoordMapper* texMapper, Material* material)
	: Solid(texMapper, material), p1(v1), p2(v2), p3(v3)
	{}


	Intersection Triangle::intersect(const Ray& ray, float previousBestDistance) const {
		Vector planeNormal = cross(p2 - p1, p3 - p1).normalize();
		float denom = dot(planeNormal, ray.d);
		if (denom >= 0 && denom <= 1e-12) {
			return Intersection::failure();
		}


		float t = dot(p1 - ray.o, planeNormal) / denom;
		if (t < 0 || t >= previousBestDistance) {
			return Intersection::failure();
		}

		Point planePoint = ray.getPoint(t);

		planeNormal = denom < 0 ? -planeNormal : planeNormal;


		Vector planeVector = cross(p2 - p1, p3 - p1);
		float dcr = dot(planeVector, -ray.d);
		Vector h = ray.o - p1;

		double lambda2 = dot(cross(h, p3 - p1), -ray.d) / dcr;
		double lambda3 = dot(cross(p2 - p1, h), -ray.d) / dcr;

		if (lambda2 >= 0.0f && lambda2 <= 1.0f && lambda3 >= 0.0f && lambda3 <= 1.0f && lambda2 + lambda3 <= 1) {
			return Intersection(t, ray, this, planeNormal, Point(1 - lambda2 - lambda3, lambda2, lambda3));
		}

		return Intersection::failure();
	}

	float Triangle::getArea() const {
		return cross(p2 - p1, p3 - p1).length() / 2;
	}

	Point Triangle::sample() const {
		float u = random(0, 1);
		float v = random(0, 1);
		if (u + v > 1) {
			u = 1 - u;
			v = 1 - v;
		}

		float newX = p1.x * (1 - u - v) + p2.x * u + p3.x * v;
		float newY = p1.y * (1 - u - v) + p2.y * u + p3.y * v;
		float newZ = p1.z * (1 - u - v) + p2.z * u + p3.z * v;

		return Point(newX, newY, newZ);
	}

	BBox Triangle::getBounds() const {
		Point minP = min(min(p1, p2), p3);
		Point maxP = max(max(p1, p2), p3);
		return BBox(minP, maxP);
	}

	bool planeBoxOverlap(const Vector& normal, const Point& p, const Vector& boxSize) {
		Vector vMin, vMax;
		for (int i = 0; i < 3; i++) {
			switch (i) {
			case 0:
				if (normal.x > 0.0f) {
					vMin.x = -boxSize.x;
					vMax.x = boxSize.x;
				}
				else {
					vMin.x = boxSize.x;
					vMax.x = -boxSize.x;
				}
				vMin.x -= p.x;
				vMax.x -= p.x;

				break;
			case 1:
				if (normal.y > 0.0f) {
					vMin.y = -boxSize.y;
					vMax.y = boxSize.y;
				} else {
					vMin.y = boxSize.y;
					vMax.y = -boxSize.y;
				}
				vMin.y -= p.y;
				vMax.y -= p.y;
				break;
			case 2:
				if (normal.z > 0.0f) {
					vMin.z = -boxSize.z;
					vMax.z = boxSize.z;
				} else {
					vMin.z = boxSize.z;
					vMax.z = -boxSize.z;
				}
				vMin.z -= p.z;
				vMax.z -= p.z;
				break;
			}
		}
		

		if (dot(normal, vMin) > 0.0f) return false;
		if (dot(normal, vMax) >= 0.0f) return true;

		return false;
	}

	bool Triangle::intersectWithBox(const BBox& box) const {
		if(box.isPointInside(p1) || box.isPointInside(p2) || box.isPointInside(p3))
			return true;
	

		Vector translationVector = Point(0,0,0) - (box.min + 0.5 * (box.max - box.min));
		Point v1 = p1 + translationVector;
		Point v2 = p2 + translationVector;
		Point v3 = p3 + translationVector;

		Vector boxSize(box.max.x - box.min.x, box.max.y - box.min.y, box.max.z - box.min.z);
		

		Vector e1 = v2 - v1;
		Vector e2 = v3 - v2;
		Vector e3 = v1 - v3;

		float fex = fabs(e1.x);
		float fey = fabs(e1.y);
		float fez = fabs(e1.z);

		float a = e1.z * v1.y- e1.y * v1.z;
		float b = e1.z * v3.y - e1.y * v3.z;
		float min = std::min(a,b);
		float max = std::max(a, b);
		float rad = fez * boxSize.y + fey * boxSize.z;
		if (min > rad || max < -rad) {
			return false;
		}

		a = -e1.z * v1.x + e1.x * v1.z;
		b = -e1.z * v3.x + e1.x * v3.z;
		min = std::min(a, b);
		max = std::max(a, b);
		rad = fez * boxSize.x + fex * boxSize.z;
		if (min > rad || max < -rad) {
			return false;
		}
		
		a = e1.y * v2.x - e1.x * v2.y;
		b = e1.y * v3.x - e1.x * v3.y;
		min = std::min(a, b);
		max = std::max(a, b);
		rad = fey * boxSize.x + fex * boxSize.y;
		if (min > rad || max < -rad) {
			return false;
		}

		fex = fabs(e2.x);
		fey = fabs(e2.y);
		fez = fabs(e2.z);

		a = e2.z * v1.y - e2.y * v1.z;
		b = e2.z * v3.y - e2.y * v3.z;
		min = std::min(a, b);
		max = std::max(a, b);
		rad = fez * boxSize.y + fey * boxSize.z;
		if (min > rad || max < -rad) {
			return false;
		}

		a = -e2.z * v1.x + e2.x * v1.z;
		b = -e2.z * v3.x + e2.x * v3.z;
		min = std::min(a, b);
		max = std::max(a, b);
		rad = fez * boxSize.x + fex * boxSize.z;
		if (min > rad || max < -rad) {
			return false;
		}

		a = e2.y * v1.x - e2.x * v1.y;
		b = e2.y * v2.x - e2.x * v2.y;
		min = std::min(a, b);
		max = std::max(a, b);
		
		rad = fey * boxSize.x + fex * boxSize.y;
		if (min>rad || max<-rad) return false;

		fex = fabs(e3.x);
		fey = fabs(e3.y);
		fez = fabs(e3.z);

		a = e3.z*v1.y - e3.y * v1.z;
		b = e3.z * v2.y - e3.y * v2.z;
		min = std::min(a,b);
		max = std::max(a, b);

		rad = fez * boxSize.y + fey * boxSize.z;

		if(min > rad || max < -rad) return false;

		a = -e2.z * v1.x + e2.x * v1.z;
		b = -e2.z * v2.x + e2.x * v2.z;
		min = std::min(a, b);
		max = std::max(a, b);

		rad = fez * abs(translationVector.x) + fex * abs(translationVector.z);
		if (min > rad || max < -rad) return false;

		a = e3.y * v1.x - e3.x * v1.y;
		b = e3.y * v2.x - e3.x * v2.y;
		min = std::min(a, b);
		max = std::max(a, b);

		rad = fey * boxSize.x + fex * boxSize.y;
		if (min>rad || max<-rad) return false;

		Point maxPoint = rt::max(rt::max(v1, v2), v3);
		Point minPoint = rt::min(rt::min(v1, v2), v3);

		if(minPoint.x > boxSize.x || maxPoint.x < -boxSize.x) return false;
		if(minPoint.y > boxSize.y || maxPoint.y < -boxSize.y) return false;
		if(minPoint.z > boxSize.z || maxPoint.z < -boxSize.z) return false;

		Vector triangleNormal = cross(e1, e2);
		if(!planeBoxOverlap(triangleNormal, v1, boxSize)) 
			return false;

		return true;
	}


	
}