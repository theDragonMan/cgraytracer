#include "core/assert.h"
#include "rt/bbox.h"
#include "rt/ray.h"
#include "rt/intersection.h"

#include "rt/solids/disc.h"

namespace rt {
	Disc::Disc(const Point& center, const Vector& normal, float radius, CoordMapper* texMapper, Material* material)
	:m_center(center), m_normal(normal.normalize()), m_radius(radius)
	{}

	Intersection Disc::intersect(const Ray& ray, float previousBestDistance) const{
		float d = dot(ray.d, m_normal);
		if (d >= 0 && d <= 1e-6) {
			return Intersection::failure();
		}

		float t = dot(m_center - ray.o, m_normal) / d;
		Point itPoint = ray.getPoint(t);


		Vector normal = dot((ray.o - m_center).normalize(), m_normal) < 0 ? -m_normal : m_normal;

		if (t >= 0 && (m_center - itPoint).lensqr() <= m_radius * m_radius && t < previousBestDistance) {
			return Intersection(t, ray, this, normal, Point(0, 0, 0));
		}

		return Intersection::failure();
	}

	float Disc::getArea() const {
		return pi * m_radius * m_radius;
	}

	BBox Disc::getBounds() const {
		NOT_IMPLEMENTED;
	}

	Point Disc::sample() const {
		NOT_IMPLEMENTED;
	}

	bool Disc::intersectWithBox(const BBox& box) const {
		
		if (box.min.x <= m_center.x && box.min.y <= m_center.y &&
			box.min.z <= m_center.z && box.max.x >= m_center.x && box.max.y >= m_center.y && box.max.z >= m_center.z) {
			return true;
		}
		float distX = box.max.x - box.min.x;
		float distY = box.max.y - box.min.y;
		float distZ = box.max.z - box.min.z;

		std::vector<Ray> rays = box.boxRays();
		for (Ray& r : rays) {
			Intersection is = intersect(r);
			if (is) {
				if(r.d == Vector(1, 0, 0)) {
					return distX >= is.distance;
				} else if (r.d == Vector(0, 1, 0)) {
					return distY >= is.distance;
				} else {
					return distZ >= is.distance;
				}
			}
		}

		return false;
	}
}
