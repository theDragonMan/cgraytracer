#include "core/assert.h"
#include "rt/bbox.h"
#include "rt/ray.h"
#include "rt/intersection.h"

#include "rt/solids/infiniteplane.h"


namespace rt {
	InfinitePlane::InfinitePlane(const Point& origin, const Vector& normal, CoordMapper* texMapper, Material* material)
	: Solid(texMapper, material), m_origin(origin), m_normal(normal.normalize())
	{}

	Intersection InfinitePlane::intersect(const Ray& ray, float previousBestDistance) const {
		float d = dot(ray.d, m_normal);

		if ( d >= 0 && d <= 1e-6) {
			return Intersection::failure();
		}

		float t = dot(m_origin - ray.o, m_normal) / d;

		Vector normal = dot((ray.o - m_origin).normalize(), m_normal) < 0 ? -m_normal : m_normal;
		if (t >= 0 && t < previousBestDistance) {
			Point textCoord = ray.getPoint(t);
			
			return Intersection(t, ray, this, normal, textCoord);
		}

		return Intersection::failure();

	}

	BBox InfinitePlane::getBounds() const {
		float dotX = roundf(dot(m_normal, Vector(1, 0, 0)));
		float dotY = roundf(dot(m_normal, Vector(0, 1, 0)));
		float dotZ = roundf(dot(m_normal, Vector(0, 0, 1)));

		if (dotX == 0.0f && dotZ == 0.0f) {
			return BBox(Point(FLT_MIN, -0.1f, FLT_MIN), Point(FLT_MAX, 0.1f, FLT_MAX));
		}

		if (dotX == 0.0f && dotY == 0.0f) {
			return BBox(Point(FLT_MIN, FLT_MIN, -0.1f), Point(FLT_MAX, FLT_MAX, 0.1f));
		}

		if (dotY == 0.0f && dotZ == 0.0f) {
			return BBox(Point(-0.1f, FLT_MIN, FLT_MIN), Point(0.1f, FLT_MAX, FLT_MAX));
		}

		return BBox::full();
	}

	bool InfinitePlane::intersectWithBox(const BBox& box) const {
		float distX = box.max.x - box.min.x;
		float distY = box.max.y - box.min.y;
		float distZ = box.max.z - box.min.z;

		std::vector<Ray> rays = box.boxRays();
		for (Ray& r : rays) {
			Intersection is = intersect(r);
			if (is) {
				if (r.d == Vector(1, 0, 0)) {
					return distX >= is.distance;
				}
				else if (r.d == Vector(0, 1, 0)) {
					return distY >= is.distance;
				}
				else {
					return distZ >= is.distance;
				}
			}
		}
	}

	Point InfinitePlane::sample() const {
		NOT_IMPLEMENTED;
	}

	float InfinitePlane::getArea() const {
		return FLT_MAX;
	}
}