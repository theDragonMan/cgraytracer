#include "core/point.h"
#include "core/vector.h"
#include "core/color.h"
#include "rt/solids/solid.h"
#include "rt/materials/material.h"
#include "arealight.h"


namespace rt {
	AreaLight::AreaLight(Solid* source)
	:m_source(source)
	{}

	LightHit AreaLight::getLightHit(const Point& p) const {
		Point areaSample = m_source->sample();
		Vector shadowRay = areaSample - p;
		LightHit lh;
		lh.distance = shadowRay.length();
		lh.direction = shadowRay.normalize();

		return lh;
	}

	RGBColor AreaLight::getIntensity(const LightHit& irr) const {
		return m_source->material->getEmission(Point(0.5, 0.5, 0), Vector(1, 1, 1), Vector(1, 1, 0)) * m_source->getArea() /  (irr.distance * irr.distance);
	}

	int AreaLight::getSamplesNumber() const {
		return 1; // for now it's just a magic number
	}
}