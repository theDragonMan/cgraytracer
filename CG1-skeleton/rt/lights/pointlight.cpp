#include <algorithm>
#include "pointlight.h"

namespace rt {
	PointLight::PointLight(const Point& position, const RGBColor& intensity) 
	:m_position(position), m_intensity(intensity), m_radius(1)
	{}

	PointLight::PointLight(const Point& position, const RGBColor& intensity, float radius)
	:m_position(position), m_intensity(intensity), m_radius(radius)
	{}

	LightHit PointLight::getLightHit(const Point& p) const {
		LightHit lh;
		lh.direction = m_position - p;
		lh.distance = lh.direction.length();
		lh.direction = lh.direction.normalize();
		return lh;
	}

	RGBColor PointLight::getIntensity(const LightHit& irr) const {
		RGBColor lightColor = m_intensity * m_radius / (irr.distance * irr.distance);
	
		return lightColor;
	}

	int PointLight::getSamplesNumber() const {
		return 1;
	}
}