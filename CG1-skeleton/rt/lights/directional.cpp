#include <cfloat>
#include "core/point.h"
#include "directional.h"

namespace rt {
	DirectionalLight::DirectionalLight(const Vector& direction, const RGBColor& color)
	:m_direction(direction.normalize()), m_color(color)
	{}

	LightHit DirectionalLight::getLightHit(const Point& p) const {
		LightHit lh;
		lh.direction = -m_direction;
		lh.distance = FLT_MAX - 1;
		return lh;
	}

	RGBColor DirectionalLight::getIntensity(const LightHit& lh) const {
		return m_color;
	}

	int DirectionalLight::getSamplesNumber() const {
		return 1;
	}
}