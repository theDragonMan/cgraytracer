#include "core/point.h"
#include "rt/ray.h"
#include "light.h"

namespace rt {
	class ProjectiveLightSource : public Light {
	private:
		Point m_position;
	public:
		ProjectiveLightSource() {}
		ProjectiveLightSource(const Point& position);
		virtual LightHit getLightHit(const Point& p) const;
		virtual RGBColor getIntensity(const LightHit& irr) const;
		virtual int		 getSamplesNumber() const;
	private:
		float computeWeight(float fx, float fy, const Point& c, float div) const;
		RGBColor computeColor(const Ray& r) const;
	};

}