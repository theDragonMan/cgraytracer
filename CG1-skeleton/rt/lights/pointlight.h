#ifndef CG1RAYTRACER_LIGHTS_POINTLIGHT_HEADER
#define CG1RAYTRACER_LIGHTS_POINTLIGHT_HEADER

#include <core/point.h>
#include <core/color.h>
#include <rt/lights/light.h>

namespace rt {

class PointLight : public Light {
	Point m_position;
	RGBColor m_intensity;
	float m_radius;
public:
	PointLight() {}
	PointLight(const Point& position, const RGBColor& intensity);
	PointLight(const Point& position, const RGBColor& intensity, float radius);

	virtual LightHit getLightHit(const Point& p) const;
    virtual RGBColor getIntensity(const LightHit& irr) const;
	virtual int		 getSamplesNumber() const;
};

}

#endif

