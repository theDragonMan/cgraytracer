#ifndef CG1RAYTRACER_LIGHTS_SPOTLIGHT_HEADER
#define CG1RAYTRACER_LIGHTS_SPOTLIGHT_HEADER

#include <core/scalar.h>
#include <core/vector.h>
#include <core/point.h>
#include <core/color.h>
#include <rt/lights/light.h>

namespace rt {

class SpotLight : public Light {
	Point m_position;
	Vector m_direction;
	RGBColor m_intensity;
	float m_angle;
	float m_exp;
public:
	SpotLight() {}
	SpotLight(const Point& position, const Vector& direction, float angle, float exp, const RGBColor& intensity);
    virtual LightHit getLightHit(const Point& p) const;
    virtual RGBColor getIntensity(const LightHit& irr) const;
	virtual int		 getSamplesNumber() const;
};

}

#endif

