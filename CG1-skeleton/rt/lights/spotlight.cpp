#include <cmath>

#include "core/assert.h"
#include "spotlight.h"

namespace rt {
	SpotLight::SpotLight(const Point& position, const Vector& direction, float angle, float exp, const RGBColor& intensity)
	:m_position(position), m_direction(direction), m_angle(angle), m_exp(exp), m_intensity(intensity)
	{}

	LightHit SpotLight::getLightHit(const Point& p) const {
		LightHit lh;
		lh.direction = m_position - p;
		lh.distance = lh.direction.length();
		lh.direction = lh.direction.normalize();
		return lh;
	}

	RGBColor SpotLight::getIntensity(const LightHit& lh) const {
		float cosDir = dot(-lh.direction, m_direction);
		float k = 0.0f;
		if(cosDir >= cos(m_angle)) {
			k = pow(cosDir, m_exp);
		} 

		return m_intensity * k / (lh.distance * lh.distance);
	}

	int SpotLight::getSamplesNumber() const {
		return 1;
	}
}