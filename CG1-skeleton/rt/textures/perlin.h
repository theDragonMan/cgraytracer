#ifndef CG1RAYTRACER_TEXTURES_PERLIN_HEADER
#define CG1RAYTRACER_TEXTURES_PERLIN_HEADER

#include<vector>
#include <core/vector.h>

#include <rt/textures/texture.h>
#include <core/color.h>
#include <core/scalar.h>


namespace rt {

    class PerlinTexture : public Texture {
	private:
		RGBColor m_color0;
		RGBColor m_color1;

		std::vector<std::pair<float, float>> m_octaves;
    public:
        PerlinTexture(const RGBColor& white, const RGBColor& black);
        void addOctave(float amplitude, float frequency);
        virtual RGBColor getColor(const Point& coord);
        virtual RGBColor getColorDX(const Point& coord);
        virtual RGBColor getColorDY(const Point& coord);
	private:
		float calcTurbulance(float x, float y, float z);
    };

}

#endif
