#ifndef  CUBE_TEXTURE_H_


#include <vector>
#include "imagetex.h"
#include "texture.h"

namespace rt {
	class CubeTexture : public Texture {
	private:
		ImageTexture* m_posX;
		ImageTexture* m_negX;

		ImageTexture* m_posY;
		ImageTexture* m_negY;

		ImageTexture* m_posZ;
		ImageTexture* m_negZ;
		enum CubeSide {
			POS_X = 0,
			NEG_X = 1,

			POS_Y = 2,
			NEG_Y = 3,

			POS_Z = 4,
			NEG_Z = 5
		};

	public:

		CubeTexture();
		CubeTexture(ImageTexture* posX, ImageTexture* negX, ImageTexture* posY, ImageTexture* negY, ImageTexture* posZ, ImageTexture* negZ);
		CubeTexture(const std::vector<ImageTexture*>& images);

		virtual ~CubeTexture();

		virtual RGBColor getColor(const Point& texCoords);
		virtual RGBColor getColorDX(const Point& texCoords);
		virtual RGBColor getColorDY(const Point& texCoords);
	};
}
#endif // ! CUBE_TEXTURE_H_
