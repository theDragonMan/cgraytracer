#include "constant.h"

namespace rt {
	ConstantTexture::ConstantTexture()
	:m_color(0, 0, 0)
	{}

	ConstantTexture::ConstantTexture(const RGBColor& color) 
	:m_color(color)
	{}

	RGBColor ConstantTexture::getColor(const Point& coord) {
		return m_color;
	}

	RGBColor ConstantTexture::getColorDX(const Point& coord) {
		return RGBColor(0, 0, 0);
	}

	RGBColor ConstantTexture::getColorDY(const Point& coord) {
		return RGBColor(0, 0, 0);
	}
}