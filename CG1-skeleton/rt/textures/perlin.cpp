#include <rt/textures/perlin.h>
#include <core/point.h>
#include <core/interpolate.h>
#include <core/scalar.h>
#include "perlin.h"
namespace rt {

namespace {
    /* returns a value in range -1 to 1 */
    float noise(int x, int y, int z) {
        int n = x + y * 57 + z * 997;
        n = (n<<13) ^ n;
        return ( 1.0f - ( (n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f);
    }

	float smooth(float t) {
		return t * t * (3 - 2 * t);
	}

	float calcTurbulance(float x, float y, float z, const std::vector<std::pair<float, float>>& octaves) {
		float k = 0;
		float totalAmplitude = 0.0f;
		for (int i = 0; i < octaves.size(); i++) {
			float x0 = (octaves[i].second * x);
			float y0 = (octaves[i].second * y);
			float z0 = (octaves[i].second * z);

			float xWeight = x0 - floor(x0);
			float yWeight = y0 - floor(y0);
			float zWeight = z0 - floor(z0);

			int x0Int = (int)floor(x0);
			int x1Int = x0Int + 1;

			int y0Int = (int)floor(y0);
			int y1Int = y0Int + 1;

			int z0Int = (int)floor(z0);
			int z1Int = z0Int + 1;

			float x0y0z0 = fabs(octaves[i].first * noise(x0Int, y0Int, z0Int));
			float x1y0z0 = fabs(octaves[i].first * noise(x1Int, y0Int, z0Int));
			float x0y1z0 = fabs(octaves[i].first * noise(x0Int, y1Int, z0Int));
			float x1y1z0 = fabs(octaves[i].first * noise(x1Int, y1Int, z0Int));

			float x0y0z1 = fabs(octaves[i].first * noise(x0Int, y0Int, z1Int));
			float x1y0z1 = fabs(octaves[i].first * noise(x1Int, y0Int, z1Int));
			float x0y1z1 = fabs(octaves[i].first * noise(x0Int, y1Int, z1Int));
			float x1y1z1 = fabs(octaves[i].first * noise(x1Int, y1Int, z1Int));

			totalAmplitude += octaves[i].first;

			k += lerp3d(x0y0z0, x1y0z0,  x0y1z0, x1y1z0,
						x0y0z1, x1y0z1, x0y1z1, x1y1z1,
						xWeight, yWeight, zWeight);
		}

		return k;
	}
}
	PerlinTexture::PerlinTexture(const RGBColor& white, const RGBColor& black)
	: m_color0(white), m_color1(black)
	{}

	void PerlinTexture::addOctave(float amplitude, float frequency) {
		m_octaves.push_back(std::pair<float, float>(amplitude, frequency));
	}

	RGBColor PerlinTexture::getColor(const Point& coord) {
		float t = calcTurbulance(coord.x, coord.y, coord.z);
		
		return lerp(m_color0, m_color1, t);
	}

	RGBColor PerlinTexture::getColorDX(const Point& coord) {
		return RGBColor(0, 0, 0);
	}

	RGBColor PerlinTexture::getColorDY(const Point& coord) {
		return RGBColor(0, 0, 0);
	}

	float PerlinTexture::calcTurbulance(float x, float y, float z) {
		float k = 0;
		for (int i = 0; i < m_octaves.size(); i++) {
			float x0 = (m_octaves[i].second * x);
			float y0 = (m_octaves[i].second * y);
			float z0 = (m_octaves[i].second * z);

			float xWeight = x0 - floor(x0);
			float yWeight = y0 - floor(y0);
			float zWeight = z0 - floor(z0);

			int x0Int = (int)floor(x0);
			int x1Int = x0Int + 1;

			int y0Int = (int)floor(y0);
			int y1Int = y0Int + 1;

			int z0Int = (int)floor(z0);
			int z1Int = z0Int + 1;

			float x0y0z0 = fabs(m_octaves[i].first * noise(x0Int, y0Int, z0Int));
			float x1y0z0 = fabs(m_octaves[i].first * noise(x1Int, y0Int, z0Int));
			float x0y1z0 = fabs(m_octaves[i].first * noise(x0Int, y1Int, z0Int));
			float x1y1z0 = fabs(m_octaves[i].first * noise(x1Int, y1Int, z0Int));

			float x0y0z1 = fabs(m_octaves[i].first * noise(x0Int, y0Int, z1Int));
			float x1y0z1 = fabs(m_octaves[i].first * noise(x1Int, y0Int, z1Int));
			float x0y1z1 = fabs(m_octaves[i].first * noise(x0Int, y1Int, z1Int));
			float x1y1z1 = fabs(m_octaves[i].first * noise(x1Int, y1Int, z1Int));

			k += lerp3d(x0y0z0, x1y0z0, x0y1z0, x1y1z0,
				x0y0z1, x1y0z1, x0y1z1, x1y1z1,
				xWeight, yWeight, zWeight);
		}

		return k;
	}
}
