#include <cmath>
#include "core/point.h"
#include "checkerboard.h"

namespace rt {
	CheckerboardTexture::CheckerboardTexture(const RGBColor& white, const RGBColor& black)
	: m_color0(white), m_color1(black)
	{}

	RGBColor CheckerboardTexture::getColor(const Point& coord) {
		int parity = (int)(floorf(coord.x) + floorf(coord.y)) % 2;
		if (parity == 0) {
			return m_color0;
		} else {
			return m_color1;
		}
	}

	RGBColor CheckerboardTexture::getColorDX(const Point& coord) {
		return RGBColor(0, 0, 0);
	}

	RGBColor CheckerboardTexture::getColorDY(const Point& coord) {
		return RGBColor(0, 0, 0);
	}
}
