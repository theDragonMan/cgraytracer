#include "core/point.h"
#include "core/interpolate.h"
#include "imagetex.h"

namespace rt {
	ImageTexture::ImageTexture()
	:textureImage(), m_wrapMode(REPEAT), m_interpolationType(BILINEAR)
	{}

	ImageTexture::ImageTexture(const Image& img, ImageTexture::BorderHandlingType bh, ImageTexture::InterpolationType i)
	: textureImage(img), m_wrapMode(bh), m_interpolationType(i)
	{}

	ImageTexture::ImageTexture(const std::string& filename, ImageTexture::BorderHandlingType bh, ImageTexture::InterpolationType i)
		: textureImage(), m_wrapMode(bh), m_interpolationType(i)
	{
		textureImage.readPNG(filename);
	}

	RGBColor ImageTexture::getColor(const Point& textCoord) {
		float u = floor(textCoord.x);
		float v = floor(textCoord.y);
		float tu = textCoord.x - u;
		float tv = textCoord.y - v;

		if (m_wrapMode == MIRROR) {
			if ((int)u % 2 == 1) {
				tu = 1 - tu;
			}

			if ((int)v % 2 == 1) {
				tv =  1 - tv;
			}
		} else if (m_wrapMode == CLAMP) {
			tu = max(0.0f, min(1.0f, textCoord.x));
			tv = max(0.0f, min(1.0f, textCoord.y));
		}

		if(m_interpolationType == NEAREST){
			int pixelX = min((uint)roundf(tu * textureImage.width()), textureImage.width() - 1);
			int pixelY = min((uint)roundf(tv * textureImage.height()), textureImage.height() - 1);

			return textureImage(pixelX, pixelY);
		} else {
			float pixelX = tu * (textureImage.width() - 1);
			float pixelY = tv * (textureImage.height() - 1);
			
			float fu = pixelX - floor(pixelX);
			float fv = pixelY - floor(pixelY);

			int pixelX1 = (int)floor(pixelX) == textureImage.width() - 1 ? floor(pixelX) : floor(pixelX) + 1;
			int pixelY1 = (int)floor(pixelY) == textureImage.width() - 1 ? floor(pixelY) : floor(pixelY) + 1;

			RGBColor interpolatedColor = lerp2d(textureImage(floor(pixelX), floor(pixelY)), textureImage(pixelX1, floor(pixelY)),
												textureImage(floor(pixelX), pixelY1), textureImage(pixelX1, pixelY1), 
												fu, fv);

			return interpolatedColor;
		}
	}

	RGBColor ImageTexture::getColorDX(const Point& textCoord) {
		float delta = 1.0f / textureImage.width();
		RGBColor p1 = getColor(Point(textCoord.x, textCoord.y, textCoord.z));
		RGBColor p2 = getColor(Point(textCoord.x + delta, textCoord.y, textCoord.z));

		return (p2 - p1) / delta;
	}

	RGBColor ImageTexture::getColorDY(const Point& textCoord) {
		float delta = 1.0f / textureImage.height();
		RGBColor p1 = getColor(Point(textCoord.x, textCoord.y, textCoord.z));
		RGBColor p2 = getColor(Point(textCoord.x, textCoord.y + delta, textCoord.z));

		return (p2 - p1) / delta;
	}
}