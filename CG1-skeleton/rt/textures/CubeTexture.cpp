#include "core/point.h"
#include "CubeTexture.h"

namespace rt {
	CubeTexture::CubeTexture()
	: m_posX(nullptr), m_negX(nullptr), m_posY(nullptr), m_negY(nullptr), m_posZ(nullptr), m_negZ(nullptr)
	{}

	CubeTexture::CubeTexture(ImageTexture* posX, ImageTexture* negX, ImageTexture* posY, ImageTexture* negY, ImageTexture* posZ, ImageTexture* negZ)
	: m_posX(posX), m_negX(negX), m_posY(posY), m_negY(negY), m_posZ(posZ), m_negZ(negZ)
	{}

	CubeTexture::CubeTexture(const std::vector<ImageTexture*>& images) {
		m_posX = images[0];
		m_negX = images[1];

		m_posY = images[2];
		m_negY = images[3];

		m_posY = images[4];
		m_negY = images[5];
	}

	CubeTexture::~CubeTexture() {
		delete m_posX;
		delete m_negX;

		delete m_posY;
		delete m_negY;

		delete m_posZ;
		delete m_negZ;
	}

	RGBColor CubeTexture::getColor(const Point& texCoords) {
		CubeSide side = (CubeSide)(int)texCoords.z;

		switch (side) {
			case POS_X:
				return m_posX->getColor(texCoords);
			case NEG_X: return m_negX->getColor(texCoords);
			case POS_Y: return m_posY->getColor(texCoords);
			case NEG_Y: return m_negY->getColor(texCoords);
			case POS_Z: return m_posZ->getColor(texCoords);
			case NEG_Z: return m_negZ->getColor(texCoords);
		}

		return RGBColor(0, 0, 0);// default case, but it never hapens.
	}

	RGBColor CubeTexture::getColorDX(const Point& texCoords) {
		return RGBColor(0, 0, 0);
	}

	RGBColor CubeTexture::getColorDY(const Point& texCoords) {
		return RGBColor(0, 0, 0);
	}
}