#include <iostream>
#include "core/vector.h"
#include "core/point.h"
#include "core/color.h"
#include "core/random.h"
#include "rt/lights/light.h"
#include "rt/intersection.h"
#include "rt/world.h"
#include "rt/primitive.h"
#include "rt/solids/solid.h"
#include "rt/materials/material.h"
#include "rt/coordmappers/coordmapper.h"

#include "recraytrace.h"

#include <iostream>

namespace rt {

	RGBColor RecursiveRayTracingIntegrator::getRadiance(const Ray& ray) const {
		return getRadiance(ray, 0);
	}

	RGBColor RecursiveRayTracingIntegrator::getRadiance(const Ray& ray, int recursionDepth) const {
		if (recursionDepth >= s_recursionDepth) {
			return RGBColor(0, 0, 0);
		}
		
		Intersection is = world->scene->intersect(ray);
		RGBColor pixelColor(0, 0, 0);
		

		if (is) {

			Point texCoords = is.solid->texMapper->getCoords(is);
			switch (is.solid->material->useSampling()) {
				case Material::Sampling::SAMPLING_NOT_NEEDED :
					pixelColor = getIrradianceFromLight(is);
					break;
				case Material::Sampling::SAMPLING_ALL:
					{
						Material::SampleReflectance sr = is.solid->material->getSampleReflectance(texCoords, is.m_normal, -is.ray.d);
						if(sr.direction != Vector(0, 0, 0)){
							Ray secondaryRay(is.hitPoint() + 1e-4 * sr.direction, sr.direction);
							pixelColor = sr.reflectance * getRadiance(secondaryRay, recursionDepth + 1);
						}
					}
					break;
				case Material::Sampling::SAMPLING_SECONDARY:
					{
						Material::SampleReflectance sr = is.solid->material->getSampleReflectance(texCoords, is.m_normal, -is.ray.d);
						Ray secondaryRay(is.hitPoint() + 1e-4 * sr.direction, sr.direction);

						pixelColor = sr.reflectance * getRadiance(secondaryRay, recursionDepth + 1);
						pixelColor = getIrradianceFromLight(is) + pixelColor;
						pixelColor = is.solid->material->getEmission(texCoords, is.m_normal, -ray.d) + pixelColor;
					}
					break;
			}
			pixelColor = pixelColor + is.solid->material->getEmission(is.uv, is.m_normal, -ray.d);
		}

		return pixelColor;
	}

	RGBColor RecursiveRayTracingIntegrator::getIrradianceFromLight(const Intersection& is) const {
		Vector cameraDir = -is.ray.d;
		RGBColor pixelColor(0, 0, 0);
		for (Light* l : world->light) {
			int samples = l->getSamplesNumber();
			RGBColor lightContribution(0, 0, 0);
			for(int i = 0; i < samples; i++){
				LightHit lh = l->getLightHit(is.hitPoint());
				float cosSurface = dot(lh.direction.normalize(), is.m_normal);
				Point texCoords = is.solid->texMapper->getCoords(is);
				if (cosSurface > 0) {
					Ray shadowRay(is.hitPoint() + lh.direction * 1e-3, lh.direction);
					Intersection lightIS = world->scene->intersect(shadowRay, lh.distance * (1 - 5e-3));
					if (!lightIS) {
						RGBColor lightIrradiance = l->getIntensity(lh);
						RGBColor materialReflectence = is.solid->material->getReflectance(texCoords, is.m_normal, cameraDir, lh.direction);
						lightContribution = lightContribution + materialReflectence * lightIrradiance * cosSurface;
					}
				}
			}
			
			lightContribution = lightContribution / samples;
			pixelColor = pixelColor + lightContribution;
		}
		float aoStrength = 0.2f;
		float aoDistance = 10.0f;
		return pixelColor + aoStrength * getAmbientOcclusion(is, aoSamples, aoDistance);
	}

	RGBColor RecursiveRayTracingIntegrator::getAmbientOcclusion(const Intersection& is, int samples, float dist) const
	{
		int notOccluded = 0;
		Vector sampleDir;

		// creating the coordinate system
		// N-normal, Nt-tangent, Nb-bitangent
		Vector N, Nt, Nb;
		N = is.normal();
		if (std::fabs(N.x) > std::fabs(N.y)) {
			Nt = Vector(N.z, 0.0f, -N.x).normalize();
		}
		else {
			Nt = Vector(0, -N.z, N.y).normalize();
		}
		Nb = cross(N, Nt);

		for (int i=0; i<samples; i++){
			sampleDir = uniformSampleHemisphere(random(), random());

			// transofrm sampleDir to Local Coordinate System
			sampleDir = Vector(sampleDir.x * Nb.x + sampleDir.y * N.x + sampleDir.z * Nt.x,
								sampleDir.x * Nb.y + sampleDir.y * N.y + sampleDir.z * Nt.y,
								sampleDir.x * Nb.z + sampleDir.y * N.z + sampleDir.z * Nt.z); 
			if (!world->scene->intersect(Ray(is.hitPoint() + sampleDir * 1e-3, sampleDir), dist)) {
				notOccluded++;
			}
		}
		
		return (((float)notOccluded/(float)samples) * RGBColor::rep(1.0f));
	}
}