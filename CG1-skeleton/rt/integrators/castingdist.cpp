#include "core/vector.h"
#include "core/point.h"
#include "core/color.h"
#include "rt/world.h"
#include "rt/intersection.h"
#include "rt/ray.h"


#include "castingdist.h"

namespace rt {
	RayCastingDistIntegrator::RayCastingDistIntegrator(World* world, const RGBColor& nearColor, float nearDist, const RGBColor& farColor, float farDist)
	: Integrator(world), nearColor(nearColor), nearDist(nearDist), farColor(farColor), farDist(farDist)
	{}

	RGBColor RayCastingDistIntegrator::getRadiance(const Ray& ray) const {
		Intersection is = world->scene->intersect(ray);
		if (is && is.distance <= farDist) {
			float cosA = dot(-ray.d, is.normal());
			float k = (farDist - is.distance) / (farDist - nearDist);
			RGBColor interpolatedColor = k * nearColor + (1 - k) * farColor;
			return cosA * interpolatedColor;
		}

		return RGBColor(0, 0, 0);
	}
}