#ifndef CG1RAYTRACER_INTEGRATORS_RECURSIVERAYTRACING_HEADER
#define CG1RAYTRACER_INTEGRATORS_RECURSIVERAYTRACING_HEADER

#include <rt/integrators/integrator.h>

namespace rt {

class World;
class Ray;
class RGBColor;

class RecursiveRayTracingIntegrator : public Integrator {
private:
	const static int s_recursionDepth = 10;
	//const static float aoStrength = 0.1f;
	//const static float aoDistance = 5.0f;
	const static int aoSamples = 10;
public:
    RecursiveRayTracingIntegrator(World* world) : Integrator(world) {}
	virtual  RGBColor getRadiance(const Ray& ray) const;
    RGBColor getRadiance(const Ray& ray, int recursionDepth) const;

private:
	RGBColor getIrradianceFromLight(const Intersection& is) const;
	RGBColor getAmbientOcclusion(const Intersection& is, int samples, float dist) const;
};

}

#endif
