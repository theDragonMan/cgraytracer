#include <iostream>
#include "core/vector.h"
#include "core/point.h"
#include "core/color.h"
#include "rt/lights/light.h"
#include "rt/intersection.h"
#include "rt/world.h"
#include "rt/primitive.h"
#include "rt/solids/solid.h"
#include "rt/materials/material.h"
#include "rt/coordmappers/coordmapper.h"

#include "raytrace.h"

namespace rt {
	RGBColor RayTracingIntegrator::getRadiance(const Ray& ray) const {
		Intersection is = world->scene->intersect(ray);
		RGBColor pixelColor(0, 0, 0);
		
		if (is) {
			Vector cameraDir = ray.o - is.hitPoint();
			cameraDir = cameraDir.normalize();
			Point textCoords = is.solid->texMapper->getCoords(is);
			for(Light* l : world->light) {
				LightHit lh = l->getLightHit(is.hitPoint());
				float cosSurface = dot(lh.direction, is.m_normal);
				if (cosSurface > 0) {
					Ray shadowRay(is.hitPoint() + lh.direction * 1e-4, lh.direction);
					Intersection lightIS = world->scene->intersect(shadowRay, lh.distance * (1 - 1e-4));
					if (!lightIS) {
						RGBColor lightIrradiance = l->getIntensity(lh);
						RGBColor materialReflectence = is.solid->material->getReflectance(textCoords, is.m_normal, cameraDir, lh.direction);
						pixelColor = pixelColor + materialReflectence * lightIrradiance * cosSurface;
					}

				} 
			}
			pixelColor = pixelColor + is.solid->material->getEmission(textCoords, is.m_normal, cameraDir);
		}

		return pixelColor;
	}
}