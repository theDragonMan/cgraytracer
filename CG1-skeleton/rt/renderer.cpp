#include <functional>
#include "core/vector.h"
#include "core/point.h"
#include "core/color.h"
#include "core/image.h"
#include "core/assert.h"
#include "core/random.h"
#include "rt/ray.h"
#include "rt/cameras/camera.h"
#include "rt/threading/threadpool.h"

#include "rt/renderer.h"
#include "rt/integrators/integrator.h"

using namespace rt;

RGBColor a1computeColor(uint x, uint y, uint width, uint height);
RGBColor a2computeColor(const Ray& r);

extern bool isRunning;

namespace rt {
	SDL_Surface* Renderer::sdl_surface = nullptr;
	SDL_Window*	  Renderer::sdl_window = nullptr;
	SDL_Renderer* Renderer::sdl_renderer = nullptr;
	bool		  Renderer::isRunning = true;
	Renderer::Renderer(Camera* camera, Integrator* integrator) 
	:cam(camera), integrator(integrator), samples(1)
	{
	}

	void Renderer::setSamples(uint samples) {
		this->samples = samples;
	}

	void Renderer::render(Image& img) {
		if(!isRunning) return;
		
		SDL_SetWindowSize(sdl_window, img.width(), img.height());
#ifdef RENDERER_MULTITHREADING
		int tileWidth = img.width() / 8;
		int tileHeight = img.height() / 8;
		RenderingThreadPool renderingPool(img.width(), img.height(), tileWidth, tileHeight, 8);
		uint samples = this->samples;
		std::queue<std::pair<int, int>> finishedQueue;
		std::queue<std::pair<int, int>>* pFinishedQueue = &finishedQueue;

		std::mutex finishedMutex;
		std::mutex* pFinishedMutex = &finishedMutex;

		std::function<void(int, int, Image&, RenderingThreadPool::ThreadObj&)> renderFunction = [this, pFinishedQueue, pFinishedMutex, samples, tileWidth, tileHeight](int x, int y, Image& img, RenderingThreadPool::ThreadObj& tObj) {
			for (uint i = 0; i < tileHeight; i++) {
				for (uint j = 0; j < tileWidth; j++) {
					RGBColor pixelColor(0, 0, 0);
					
					for(uint sampleCount = 0; sampleCount < samples; sampleCount++){
						float offsetX = sampleCount <= 1 ? 0.5f : random(0, 1);
						float offsetY = sampleCount <= 0 ? 0.5f : random(0, 1);

						float screenX = 2 * (x + j + offsetX) / img.width() - 1;
						float screenY = 1 - 2 * (y + i + offsetY) / img.height();

						RGBColor sampleColor = ComputeColor(screenX, screenY);
						pixelColor = pixelColor + sampleColor;
					}

					
					pixelColor = pixelColor / this->samples;
					pixelColor.r = std::min(1.0f, std::max(0.0f, pixelColor.r));
					pixelColor.g = std::min(1.0f, std::max(0.0f, pixelColor.g));
					pixelColor.b = std::min(1.0f, std::max(0.0f, pixelColor.b));
					img(x + j, y + i) = pixelColor;
					while(tObj.m_state == RenderingThreadPool::THREAD_SLEEPING) SDL_Delay(10);
					if(tObj.m_state == RenderingThreadPool::THREAD_DYING) break;
				}
			}
			std::lock_guard<std::mutex> guard(*pFinishedMutex);
			pFinishedQueue->push(std::pair<int, int>(x, y));
		};

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				renderingPool.addRenderingTask(j * tileWidth, i * tileHeight);
			}
		}

		std::mutex& poolMutex = renderingPool.getPoolMutex();
		renderingPool.start(renderFunction, img);
		while (!renderingPool.isPoolFinished) {
			SDL_Event e;
			while (SDL_PollEvent(&e)) {
				if (e.type == SDL_QUIT) {
					renderingPool.killAllThreads();
					isRunning = false;
				}
			}

			std::lock_guard<std::mutex> guard(finishedMutex);
			while (!finishedQueue.empty()) {
				std::pair<int, int> tile = finishedQueue.front();
				finishedQueue.pop();
				for (int i = 0; i < tile.second + tileHeight; i++) {
					for (int j = tile.first; j < tile.first + tileWidth; j++) {
						ChangePixelColor(j, i, img(j, i));
					}
				}
				
			}
			SDL_RenderPresent(sdl_renderer);
		}
#else 
		for (uint i = 0; i < img.height(); i++) {
			for (uint j = 0; j < img.width(); j++) {
				RGBColor pixelColor(0, 0, 0);
				int sampleCount = 0;
				for(; sampleCount < samples; sampleCount++){
					float offsetX = sampleCount == 0 ? 0.5f : random(0, 1);
					float offsetY = sampleCount == 0 ? 0.5f : random(0, 1);

					float screenX = 2 * (j + offsetX) / img.width() - 1;
					float screenY = 1 - 2 * (i + offsetY) / img.height();
					
					RGBColor sampleColor = ComputeColor(screenX, screenY);
					sampleColor.r = std::min(1.0f, std::max(0.0f, sampleColor.r));
					sampleColor.g = std::min(1.0f, std::max(0.0f, sampleColor.g));
					sampleColor.b = std::min(1.0f, std::max(0.0f, sampleColor.b));
				
					pixelColor = pixelColor + sampleColor;
				}

				pixelColor = pixelColor / sampleCount;

				img(j, i) = pixelColor;
				if (sdl_surface != nullptr) {
					ChangePixelColor(j, i, pixelColor);
				}
			}

			SDL_Event e;
			while (SDL_PollEvent(&e)) {
				if (e.type == SDL_QUIT) {
					isRunning = false;
				}
			}
			SDL_RenderPresent(sdl_renderer);

			if (!isRunning) {
				//		SDL_Quit();
				return;
			}
		}
#endif
	}

	void Renderer::test_render1(Image& img) {
		for (uint i = 0; i < img.height(); i++) {
			for (uint j = 0; j < img.width(); j++) {
				RGBColor pixelColor = a1computeColor(j, i, img.width(), img.height());
				img(j, i) = pixelColor;
				if (sdl_surface != nullptr) {
					ChangePixelColor(j, i, pixelColor);
				}
				
			}
		}
	}

	void Renderer::test_render2(Image& img) {
		for (uint i = 0; i < img.height(); i++) {
			for (uint j = 0; j < img.width(); j++) {
				float aspect = img.width() / img.height();

				float screenX = 2 * (j + 0.5f) / img.width() - 1;
				float screenY = 1 - 2 * (i + 0.5f) / img.height();
				Ray cameraRay = cam->getPrimaryRay(screenX, screenY);
				RGBColor pixelColor = a2computeColor(cameraRay);
				img(j, i) = pixelColor;
				if (sdl_surface != nullptr) {
					ChangePixelColor(j, i, pixelColor);
				}

			}
		}
	}

	RGBColor Renderer::ComputeColor(float x, float y) const {
		Ray primaryRay = cam->getPrimaryRay(x, y);
		RGBColor pixelColor = integrator->getRadiance(primaryRay);
		return pixelColor;
	}


	void Renderer::ChangePixelColor(uint x, uint y, const RGBColor& color) const {
		SDL_SetRenderDrawColor(sdl_renderer, (uint)(color.r * 255), (uint)(color.g * 255), (uint)(color.b * 255), 255);
		SDL_RenderDrawPoint(sdl_renderer, x, y);
	}


}
