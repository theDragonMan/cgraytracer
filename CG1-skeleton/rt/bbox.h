#ifndef CG1RAYTRACER_BBOX_HEADER
#define CG1RAYTRACER_BBOX_HEADER

#include <utility>
#include <vector>
#include <core/point.h>
#include <core/vector.h>

namespace rt {

class Ray;
class Primitive;
class BBox {
public:
    Point min, max;

    BBox() {}
    BBox(const Point& min, const Point& max) : min(min), max(max) {}
    static BBox empty();
    static BBox full();

    void extend(const Point& point);
    void extend(const BBox& bbox);

    Vector diagonal() const;
    std::pair<float,float> intersect(const Ray& ray) const;

	float getArea() const;

	std::vector<Ray> boxRays() const;
	bool isPointInside(const Point& p) const;
    bool isUnbound();
};

}


#endif
