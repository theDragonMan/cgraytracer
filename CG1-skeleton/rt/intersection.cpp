#include "core/assert.h"
#include "rt/solids/solid.h"

#include "rt/intersection.h"

namespace rt {
	Intersection Intersection::failure() {
		Intersection it(FLT_MAX, Ray(), nullptr, Vector(0, 0, 0), Point(0, 0, 0));

		return it;
	}

	Intersection::Intersection(float distance, const Ray& r, const Solid* solid, const Vector& normal, const Point& uv)
	: distance(distance), ray(r), solid(solid), m_normal(normal), wasIntersected(false), uv(uv)
	{}

	Point Intersection::hitPoint() const{
		return ray.o + distance * ray.d;
	}

	Vector Intersection::normal() const {
		return m_normal;
	}

	Point Intersection::local() const {
		return uv;
	}

	Intersection::operator bool(){
		return solid != nullptr;
	}

}
