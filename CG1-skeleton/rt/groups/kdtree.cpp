
#include <algorithm>
#include <stack>
#include <tuple>
#include "core/vector.h"
#include "core/point.h"
#include "core/assert.h"
#include "core/random.h"

#include "rt/groups/simplegroup.h"
#include "rt/intersection.h"
#include "rt/ray.h"

#include "kdtree.h"

#ifdef DEBUG_KD_TREE
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#endif
namespace rt {

	KDTree::Node::Node(Axis axis, float value, Node* left, Node* right)
	:m_axis(axis), m_value(value), m_left(left), m_right(right), m_elements(nullptr)
	{}

	KDTree::Node::~Node() {
		if (m_elements) {
			delete m_elements;
		}
	}

	bool KDTree::Node::isLeaf() const {
		return m_axis == A_NONE;
	}

	KDTree::KDTree()
	:m_root(nullptr), m_box(BBox::empty())
	{}

	KDTree::~KDTree() {
		destroyTree(m_root);
		for (Primitive* p : m_elements) {
			delete p;
		}
	}


	Intersection KDTree::intersect(const Ray& ray, float previousBestDistance) const {
		std::pair<float, float> isPoints = m_box.intersect(ray);
		if (isPoints.first > isPoints.second) {
			return Intersection::failure();
		}

		std::stack<KDTuple> treeStack;
		treeStack.push(KDTuple(nullptr, m_root, isPoints.first, isPoints.second));

		Node* previousNode = nullptr;
		float bestDistance = previousBestDistance;
		int depth = 0;
		
		bool shouldRecord = false;
		while(!treeStack.empty()){
			KDTuple kdTuple = treeStack.top();
			treeStack.pop();
			previousNode = std::get<0>(kdTuple);
			Node* currentNode = std::get<1>(kdTuple);
			float tNear = std::get<2>(kdTuple);
			float tFar = std::get<3>(kdTuple);

			while (currentNode && !currentNode->isLeaf()) {
				depth++;
				float originAxisValue;
				float dirAxisValue;
				previousNode = currentNode;
				switch (currentNode->m_axis) {
					case A_X:
						originAxisValue = ray.o.x;
						dirAxisValue = ray.d.x;
						break;
					case A_Y:
						originAxisValue = ray.o.y;
						dirAxisValue = ray.d.y;
						break;
					case A_Z:
						originAxisValue = ray.o.z;
						dirAxisValue = ray.d.z;
						break;
				}


				float tSplit = (currentNode->m_value - originAxisValue) / dirAxisValue;


				Node* nearChild;
				Node* farChild;
				if (originAxisValue < currentNode->m_value || (originAxisValue == currentNode->m_value && dirAxisValue <= 0)) {
					nearChild = currentNode->m_left;
					farChild = currentNode->m_right;
				}
				else {
					nearChild = currentNode->m_right;
					farChild = currentNode->m_left;
				}

				if (tFar <= tSplit || (tSplit < tNear && tSplit < 0) || (tSplit == 0 && dirAxisValue < 0) || (tSplit < 0 && tNear < tSplit)) {
					currentNode = nearChild;
					continue;
				}

				if (tSplit <= tNear || (tSplit == 0 && dirAxisValue > 0)) {
					currentNode = farChild;
					continue;
				}

				

				treeStack.push(KDTuple(previousNode, farChild, tSplit, tFar));
				
				currentNode = nearChild;
				tFar = tSplit;
			}
		
			if (currentNode) {
				Intersection is = currentNode->m_elements->intersect(ray, bestDistance);
				if(!previousNode) return is;
				
				if (is) {
					float axisValue = ray.o[previousNode->m_axis] + ray.d[previousNode->m_axis] * is.distance;;

					if ((currentNode == previousNode->m_left && axisValue <= previousNode->m_value) || (currentNode == previousNode->m_right && axisValue > previousNode->m_value)) {
						return is;
					}
				
				}
			}
		}
		return Intersection::failure();
	}

	void KDTree::add(Primitive* p) {
		m_elements.push_back(p);
		m_box.extend(p->getBounds());
	}

	BBox KDTree::getBounds() const {
		return m_box;
	}

	void KDTree::setMaterial(Material* m) {
		for (Primitive* p : m_elements) {
			p->setMaterial(m);
		}
	}

	void KDTree::setCoordMapper(CoordMapper* coordMap) {
		NOT_IMPLEMENTED;
	}

	void KDTree::rebuildIndex() {
		m_root = buildSubtree(m_box, m_elements);
	}

	KDTree::Node* KDTree::buildSubtree(const BBox& levelBounds, const std::vector<Primitive*>& elements, int depth) {
		if(elements.size() == 0) return nullptr;

		Node* currentNode = new Node();
		if (elements.size() <= 20 || depth > 37) {
			currentNode->m_axis = Axis::A_NONE;
			currentNode->m_elements = new SimpleGroup();
			for (Primitive* p : elements) {
				currentNode->m_elements->add(p);
			}
		} else {
			//float bestSplitValue = 0;
			//Axis bestSplitAxis = A_NONE;
			//float cost = FLT_MAX;
			//BBox leftBBox;
			//BBox rightBBox;
			//float levelArea = levelBounds.getArea();
			//for (int i = 0; i < 50; i++) {
			//	for (int j = 0; j < 3; j++) {
			//		Axis currentAxis = (Axis)j;
			//		leftBBox = levelBounds;
			//		rightBBox = levelBounds;
			//		float splitPlane = random(levelBounds.min[j], levelBounds.max[j]);
			//		leftBBox.max[j] = splitPlane;
			//		rightBBox.min[j] = splitPlane;
			//		int leftChildCount = 0;
			//		int rightChildCount = 0;
			//		for (Primitive* p : elements) {
			//			if (p->intersectWithBox(leftBBox)) {
			//				leftChildCount++;
			//			}

			//			if (p->intersectWithBox(rightBBox)) {
			//				rightChildCount++;
			//			}
			//		}

			//		float leftSA = leftBBox.getArea();
			//		float rightSA = rightBBox.getArea();
			//		float lambda = leftChildCount == 0 || rightChildCount == 0 ? 0.8 : 1;
			//		float sah = lambda * (2 + leftSA/levelArea * leftChildCount + rightSA / levelArea * rightChildCount);
			//		if (sah < cost) {
			//			cost = sah;
			//			bestSplitAxis = (Axis)j;
			//			bestSplitValue = splitPlane;
			//		}
			//	}
			//}

			//leftBBox = levelBounds;
			//rightBBox = levelBounds;
			//leftBBox.max[bestSplitAxis] = bestSplitValue;
			//rightBBox.min[bestSplitAxis] = bestSplitValue;
			std::vector<Primitive*> leftChildren;
			std::vector<Primitive*> rightChildren;

			Axis a = (Axis)(depth % 3);
			
			BBox leftBBox = levelBounds;
			BBox rightBBox = levelBounds;
			float median;


			switch (a) {
				case Axis::A_X:
					{
						float splitX = (levelBounds.max.x + levelBounds.min.x) / 2;
						leftBBox.max.x = splitX;
						rightBBox.min.x = splitX;
						median = splitX;
					}
					break;
				case Axis::A_Y:
					{
						float splitY = (levelBounds.max.y + levelBounds.min.y) / 2;
						leftBBox.max.y = splitY;
						rightBBox.min.y = splitY;
						median = splitY;
					}
					break;
				case Axis::A_Z:
					{
						float splitZ = (levelBounds.max.z + levelBounds.min.z) / 2;
						leftBBox.max.z = splitZ;
						rightBBox.min.z = splitZ;
						median = splitZ;
					}
					break;	
			}

			
			for (Primitive* p : elements) {
				if (p->intersectWithBox(leftBBox)) {
					leftChildren.push_back(p);
				}

				if (p->intersectWithBox(rightBBox)) {
					rightChildren.push_back(p);
				}
			}

			Node* leftChild = buildSubtree(leftBBox, leftChildren, depth + 1);
			Node* rightChild = buildSubtree(rightBBox, rightChildren, depth + 1);
	
			currentNode->m_axis = a;
			currentNode->m_value = median;
			currentNode->m_left = leftChild;
			currentNode->m_right = rightChild;
		}

		return currentNode;
	}

	void KDTree::destroyTree(Node* node) {
		if(!node) return;

		if (node->m_axis != A_NONE) {
			destroyTree(node->m_left);
			destroyTree(node->m_right);
		}
		delete node;
	}

	bool KDTree::intersectWithBox(const BBox& box) const {
		float distX = box.max.x - box.min.x;
		float distY = box.max.y - box.min.y;
		float distZ = box.max.z - box.min.z;

		std::vector<Ray> rays = box.boxRays();
		for (Ray& r : rays) {
			Intersection is = intersect(r);
			if (is) {
				if (r.d == Vector(1, 0, 0)) {
					return distX >= is.distance;
				}
				else if (r.d == Vector(0, 1, 0)) {
					return distY >= is.distance;
				}
				else {
					return distZ >= is.distance;
				}
			}
		}
	}

}