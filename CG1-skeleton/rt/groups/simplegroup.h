#ifndef CG1RAYTRACER_GROUPS_SIMPLEGROUP_HEADER
#define CG1RAYTRACER_GROUPS_SIMPLEGROUP_HEADER

#include <vector>
#include <rt/groups/group.h>
#include "rt/bbox.h"

namespace rt {

class SimpleGroup : public Group {
friend class KDTree;
private:
	Primitives	m_objects;
	
	BBox	m_box;
public:
	virtual ~SimpleGroup();

    virtual BBox getBounds() const;

    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
    virtual void rebuildIndex();
    virtual void add(Primitive* p);
    virtual void setMaterial(Material* m);
    virtual void setCoordMapper(CoordMapper* cm);
	virtual bool intersectWithBox(const BBox& box) const;

private:
	BBox& getMutableBBox();
};

}

#endif
