#include "core/assert.h"
#include "rt/ray.h"
#include "rt/intersection.h"
#include "rt/bbox.h"

#include "simplegroup.h"

namespace rt {
	

	SimpleGroup::~SimpleGroup() {
		/*for (Primitive* p : m_objects) {
			delete p;
			p = nullptr;
		}*/
	}

	Intersection SimpleGroup::intersect(const Ray& ray, float previousBestDistance) const {
		Intersection bestIS = Intersection::failure();
		for (Primitive* p : m_objects) {
			Intersection is = p->intersect(ray, previousBestDistance);
			if (is) {
				bestIS = is;
				previousBestDistance = is.distance;
			}
		}

		return bestIS;
	}

	BBox SimpleGroup::getBounds() const {
		return m_box;
	}

	void SimpleGroup::rebuildIndex() {
//		NOT_IMPLEMENTED;
	}

	void SimpleGroup::add(Primitive* p) {
		m_objects.push_back(p);
		m_box.extend(p->getBounds());
	}

	void SimpleGroup::setCoordMapper(CoordMapper* cMapper) {
		NOT_IMPLEMENTED;
	}

	void SimpleGroup::setMaterial(Material* cMapper) {
		for (Primitive* p : m_objects) {
			p->setMaterial(cMapper);
		}
	}

	BBox& SimpleGroup::getMutableBBox() {
		return m_box;
	}

	bool SimpleGroup::intersectWithBox(const BBox& box) const {
		float distX = box.max.x - box.min.x;
		float distY = box.max.y - box.min.y;
		float distZ = box.max.z - box.min.z;

		std::vector<Ray> rays = box.boxRays();
		for (Ray& r : rays) {
			Intersection is = intersect(r);
			if (is) {
				if (r.d == Vector(1, 0, 0)) {
					return distX >= is.distance;
				}
				else if (r.d == Vector(0, 1, 0)) {
					return distY >= is.distance;
				}
				else {
					return distZ >= is.distance;
				}
			}
		}
	}
}