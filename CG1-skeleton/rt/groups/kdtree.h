#ifndef CG1RAYTRACER_GROUPS_KDTREE_HEADER
#define CG1RAYTRACER_GROUPS_KDTREE_HEADER

#include <vector>
#include <rt/groups/group.h>
#include <rt/bbox.h>

namespace rt {

class SimpleGroup;

class KDTree : public Group {
private:
	enum Axis {
		A_NONE = -1,
		A_X = 0,
		A_Y = 1,
		A_Z = 2
	};

	struct Node {
		Axis m_axis;
		float	m_value;
		Node*	m_left;
		Node*	m_right;
		SimpleGroup* m_elements;

		Node(Axis m_axis = A_NONE, float val = FLT_MIN, Node* left = nullptr, Node* right = nullptr);
		~Node();
		bool isLeaf() const;
	};

	typedef std::tuple<KDTree::Node*, KDTree::Node*, float, float> KDTuple;

private:
	BBox m_box;
	Node* m_root;
	Primitives m_elements;

public:
    KDTree();
    virtual BBox getBounds() const;
    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
    virtual void rebuildIndex();
	virtual ~KDTree();
    virtual void add(Primitive* p);
    virtual void setMaterial(Material* m);
    virtual void setCoordMapper(CoordMapper* cm);

	virtual bool intersectWithBox(const BBox& box) const;
private:
	void destroyTree(Node* node);
	Node* buildSubtree(const BBox& bound, const std::vector<Primitive*>& elements, int depth = 0);

};

}

#endif
