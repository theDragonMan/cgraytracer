#include "core/assert.h"
#include "core/float4.h"
#include "core/color.h"


namespace rt {
	RGBColor::RGBColor(const Float4& f4) {
		r = f4.x / f4.w;
		g = f4.y / f4.w;
		b = f4.z / f4.w;
	}

	RGBColor RGBColor::operator+(const RGBColor& c) const {
		return RGBColor(r + c.r, g + c.g, b + c.b);
	}

	RGBColor RGBColor::operator-(const RGBColor& c) const {
		return RGBColor(r - c.r, g - c.g, b - c.b);
	}

	RGBColor RGBColor::operator*(const RGBColor& c) const {
		return RGBColor(r * c.r, g * c.g, b * c.b);
	}

	bool RGBColor::operator==(const RGBColor& c) const {
		return r == c.r && g == c.g && b == c.b;
	}

	bool RGBColor::operator!=(const RGBColor& c) const {
		return !(*this == c);
	}

	RGBColor RGBColor::clamp() const {
		return *this;
	}

	RGBColor RGBColor::gamma(float gam) const {
		NOT_IMPLEMENTED;
	}

	float RGBColor::luminance() const {
		NOT_IMPLEMENTED;
	}

	RGBColor operator*(const RGBColor& color, float scalar) {
		return RGBColor(color.r * scalar, color.g * scalar, color.b * scalar);
	}

	RGBColor operator*(float scalar, const RGBColor& color) {
		return RGBColor(color.r * scalar, color.g * scalar, color.b * scalar);
	}

	RGBColor operator/(const RGBColor& color, float scalar) {
		return RGBColor(color.r / scalar, color.g / scalar, color.b / scalar);
	}
}
