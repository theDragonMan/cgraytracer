#include <algorithm>
#include <cmath>

#include "core/vector.h"
#include "core/point.h"
#include "core/float4.h"
#include "core/assert.h"

using std::min;
using std::max;

namespace rt{
	Float4::Float4(float a, float b, float c, float d)
	: x(a), y(b), z(c), w(d)
	{}

	Float4::Float4(const Vector& v) 
	: x(v.x), y(v.y), z(v.z), w(0)
	{}

	Float4::Float4(const Point& p)
	: x(p.x), y(p.y), z(p.z), w(1)
	{}

	float& Float4::operator[](int index) {
		{ release_assert(index < 4 && index >= 0) << "Index out of range"; }

		switch (index) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
			case 3: return w;
		}
	}

	float Float4::operator[](int index) const {
		{ release_assert(index < 4 && index >= 0) << "Index out of range"; }

		switch (index) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
			case 3: return w;
		}
	}

	Float4 Float4::operator+(const Float4& a) const {
		return Float4(x + a.x, y + a.y, z + a.z, w + a.w);
	}

	Float4 Float4::operator-(const Float4& a) const {
		return Float4(x - a.x, y - a.y, z - a.z, w - a.w);
	}

	Float4 Float4::operator*(const Float4& c) const {
		return Float4(x * c.x, x * c.x, x * c.x, w * c.w);
	}

	Float4 Float4::operator/(const Float4& c) const {
		return Float4(x / c.x, x / c.x, x / c.x, w / c.w);
	}

	Float4 Float4::operator-() const {
		return Float4(-x, -y, -z, -w);
	}

	bool Float4::operator==(const Float4& c) const {
		return x == c.x && y == c.y && z == c.z && w == c.w;
	}

	bool Float4::operator!=(const Float4& c) const {
		return !(*this == c);
	}

	Float4 operator*(float scalar, const Float4& b) {
		return Float4(scalar * b.x, scalar * b.y, scalar * b.z, scalar * b.w);
	}

	float dot(const Float4& a, const Float4& b) {
		return a.x * b.x + a.y * b.y + a.z * b.z + a.w*b.w;
	}

	Float4 min(const Float4& a, const Float4& b) {
		return Float4(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z), std::min(a.w, b.w));
	}

	Float4 max(const Float4& a, const Float4& b) {
		return Float4(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z), std::max(a.w, b.w));
	}
}
