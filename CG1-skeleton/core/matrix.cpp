#include <core/matrix.h>
#include "core/assert.h"
#include "core/vector.h"
#include "core/point.h"

namespace rt {

	Matrix Matrix::zero() {
		Float4 r1 = Float4::rep(0);
		Float4 r2 = Float4::rep(0);
		Float4 r3 = Float4::rep(0);
		Float4 r4 = Float4::rep(0);
		return Matrix(r1, r2, r3, r4);
	}

	Matrix Matrix::identity() {
		Float4 r1(1, 0, 0, 0);
		Float4 r2(0, 1, 0, 0);
		Float4 r3(0, 0, 1, 0);
		Float4 r4(0, 0, 0, 1);

		return Matrix(r1, r2, r3, r4);
	}

	Matrix Matrix::system(const Vector& e1, const Vector& e2, const Vector& e3) {
		Float4 r1(e1.x, e2.x, e3.x, 0);
		Float4 r2(e1.y, e2.y, e3.y, 0);
		Float4 r3(e1.z, e2.z, e3.z, 0);
		Float4 r4(0, 0, 0, 1);
		return Matrix(r1, r2, r3, r4);
	}

	Matrix::Matrix(const Float4& r1, const Float4& r2, const Float4& r3, const Float4& r4)
	:r1(r1), r2(r2), r3(r3), r4(r4)
	{}

	Matrix Matrix::operator+(const Matrix& b) const {
		return Matrix(r1 + b.r1, r2 + b.r2, r3 + b.r3, r4 + b.r4);
	}

	Matrix Matrix::operator-(const Matrix& b) const {
		return Matrix(r1 - b.r1, r2 - b.r2, r3 - b.r3, r4 - b.r4);
	}

	Float4& Matrix::operator[](int idx) {
		switch (idx) {
			case 0: return r1;
			case 1: return r2;
			case 2: return r3;
			case 3: return r4;
		}

		UNREACHABLE;
	}

	Float4 Matrix::operator[](int idx) const {
		switch (idx) {
		case 0: return r1;
		case 1: return r2;
		case 2: return r3;
		case 3: return r4;
		}

		UNREACHABLE;
	}

	bool Matrix::operator==(const Matrix& b) const{
		return r1 == b.r1 && r2 == b.r2 && r3 == b.r3 && r4 == b.r4;
	}

	bool Matrix::operator!=(const Matrix& b) const {
		return !(*this == b);
	}

	Matrix Matrix::transpose() const {
		Float4 tr1(r1.x, r2.x, r3.x, r4.x);
		Float4 tr2(r1.y, r2.y, r3.y, r4.y);
		Float4 tr3(r1.z, r2.z, r3.z, r4.z);
		Float4 tr4(r1.w, r2.w, r3.w, r4.w);

		return Matrix(tr1, tr2, tr3, tr4);
	}

	
	Float4 Matrix::operator*(const Float4& b) const{
		Float4 f4;
		f4.x = dot(r1, b);
		f4.y = dot(r2, b);
		f4.z = dot(r3, b);
		f4.w = dot(r4, b);
		return f4;
	}

	Vector Matrix::operator*(const Vector& b) const {
		Float4 f4(b);
		f4 = *this * f4;
		return Vector(f4.x, f4.y, f4.z);
	}

	Point Matrix::operator*(const Point& b) const {
		Float4 f4(b);
		return Point(*this * f4);
	}

	Matrix Matrix::invert() const {
		Matrix result;
		const Matrix& m = *this;

		//Taken and modified from http://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
		result[0][0] = m[1][1] * m[2][2] * m[3][3] - m[1][1] * m[2][3] * m[3][2] - m[2][1] * m[1][2] * m[3][3] + m[2][1] * m[1][3] * m[3][2] + m[3][1] * m[1][2] * m[2][3] - m[3][1] * m[1][3] * m[2][2];
		result[1][0] = -m[1][0] * m[2][2] * m[3][3] + m[1][0] * m[2][3] * m[3][2] + m[2][0] * m[1][2] * m[3][3] - m[2][0] * m[1][3] * m[3][2] - m[3][0] * m[1][2] * m[2][3] + m[3][0] * m[1][3] * m[2][2];
		result[2][0] = m[1][0] * m[2][1] * m[3][3] - m[1][0] * m[2][3] * m[3][1] - m[2][0] * m[1][1] * m[3][3] + m[2][0] * m[1][3] * m[3][1] + m[3][0] * m[1][1] * m[2][3] - m[3][0] * m[1][3] * m[2][1];
		result[3][0] = -m[1][0] * m[2][1] * m[3][2] + m[1][0] * m[2][2] * m[3][1] + m[2][0] * m[1][1] * m[3][2] - m[2][0] * m[1][2] * m[3][1] - m[3][0] * m[1][1] * m[2][2] + m[3][0] * m[1][2] * m[2][1];

		float det = m[0][0] * result[0][0] + m[0][1] * result[1][0] + m[0][2] * result[2][0] + m[0][3] * result[3][0];
		if (det == 0)
			return Matrix::zero();

		result[0][1] = -m[0][1] * m[2][2] * m[3][3] + m[0][1] * m[2][3] * m[3][2] + m[2][1] * m[0][2] * m[3][3] - m[2][1] * m[0][3] * m[3][2] - m[3][1] * m[0][2] * m[2][3] + m[3][1] * m[0][3] * m[2][2];
		result[1][1] = m[0][0] * m[2][2] * m[3][3] - m[0][0] * m[2][3] * m[3][2] - m[2][0] * m[0][2] * m[3][3] + m[2][0] * m[0][3] * m[3][2] + m[3][0] * m[0][2] * m[2][3] - m[3][0] * m[0][3] * m[2][2];
		result[2][1] = -m[0][0] * m[2][1] * m[3][3] + m[0][0] * m[2][3] * m[3][1] + m[2][0] * m[0][1] * m[3][3] - m[2][0] * m[0][3] * m[3][1] - m[3][0] * m[0][1] * m[2][3] + m[3][0] * m[0][3] * m[2][1];
		result[3][1] = m[0][0] * m[2][1] * m[3][2] - m[0][0] * m[2][2] * m[3][1] - m[2][0] * m[0][1] * m[3][2] + m[2][0] * m[0][2] * m[3][1] + m[3][0] * m[0][1] * m[2][2] - m[3][0] * m[0][2] * m[2][1];
		result[0][2] = m[0][1] * m[1][2] * m[3][3] - m[0][1] * m[1][3] * m[3][2] - m[1][1] * m[0][2] * m[3][3] + m[1][1] * m[0][3] * m[3][2] + m[3][1] * m[0][2] * m[1][3] - m[3][1] * m[0][3] * m[1][2];
		result[1][2] = -m[0][0] * m[1][2] * m[3][3] + m[0][0] * m[1][3] * m[3][2] + m[1][0] * m[0][2] * m[3][3] - m[1][0] * m[0][3] * m[3][2] - m[3][0] * m[0][2] * m[1][3] + m[3][0] * m[0][3] * m[1][2];
		result[2][2] = m[0][0] * m[1][1] * m[3][3] - m[0][0] * m[1][3] * m[3][1] - m[1][0] * m[0][1] * m[3][3] + m[1][0] * m[0][3] * m[3][1] + m[3][0] * m[0][1] * m[1][3] - m[3][0] * m[0][3] * m[1][1];
		result[3][2] = -m[0][0] * m[1][1] * m[3][2] + m[0][0] * m[1][2] * m[3][1] + m[1][0] * m[0][1] * m[3][2] - m[1][0] * m[0][2] * m[3][1] - m[3][0] * m[0][1] * m[1][2] + m[3][0] * m[0][2] * m[1][1];
		result[0][3] = -m[0][1] * m[1][2] * m[2][3] + m[0][1] * m[1][3] * m[2][2] + m[1][1] * m[0][2] * m[2][3] - m[1][1] * m[0][3] * m[2][2] - m[2][1] * m[0][2] * m[1][3] + m[2][1] * m[0][3] * m[1][2];
		result[1][3] = m[0][0] * m[1][2] * m[2][3] - m[0][0] * m[1][3] * m[2][2] - m[1][0] * m[0][2] * m[2][3] + m[1][0] * m[0][3] * m[2][2] + m[2][0] * m[0][2] * m[1][3] - m[2][0] * m[0][3] * m[1][2];
		result[2][3] = -m[0][0] * m[1][1] * m[2][3] + m[0][0] * m[1][3] * m[2][1] + m[1][0] * m[0][1] * m[2][3] - m[1][0] * m[0][3] * m[2][1] - m[2][0] * m[0][1] * m[1][3] + m[2][0] * m[0][3] * m[1][1];
		result[3][3] = m[0][0] * m[1][1] * m[2][2] - m[0][0] * m[1][2] * m[2][1] - m[1][0] * m[0][1] * m[2][2] + m[1][0] * m[0][2] * m[2][1] + m[2][0] * m[0][1] * m[1][2] - m[2][0] * m[0][2] * m[1][1];

		result = result*(1.0f/det);
		return result;
	}

	float Matrix::det() const {
		float d1 = r2[1] * r3[2] * r4[3] + r2[2] * r3[3] * r4[1] + r2[3] * r3[1] * r4[2] - r2[3] * r3[2] * r4[1] - r2[1] * r3[3] * r4[2] - r2[2] * r3[1] * r4[3];
		float d2 = r2[0] * r3[2] * r4[3] + r2[2] * r3[3] * r4[0] + r2[3] * r3[0] * r4[2] - r2[3] * r3[2] * r4[0] - r2[0] * r3[3] * r4[2] - r2[2] * r3[0] * r4[3];
		float d3 = r2[0] * r3[1] * r4[3] + r2[1] * r3[3] * r4[0] + r2[3] * r3[0] * r4[1] - r2[3] * r3[1] * r4[0] - r2[0] * r3[3] * r4[1] - r2[1] * r3[0] * r4[3];
		float d4 = r2[0] * r3[1] * r4[2] + r2[1] * r3[2] * r4[0] + r2[2] * r3[0] * r4[1] - r2[2] * r3[1] * r4[0] - r2[0] * r3[2] * r4[1] - r2[1] * r3[0] * r4[2];
		return r1[0] * d1 - r1[1] * d2 + r1[2] * d3 - r1[3] * d4;
	}

	Matrix product(const Matrix& a, const Matrix& b) {
		Matrix c;
		Matrix bTransposed = b.transpose();
		for (int i = 0; i < 4; i++) {			
			for(int k = 0; k < 4; k++) {
				c[i][k] = dot(a[i], bTransposed[k]);
			}
		}
		return c;
	}

	Matrix operator*(float scalar, const Matrix& b) {
		Float4 r1 = b.r1 * scalar;
		Float4 r2 = b.r2 * scalar;
		Float4 r3 = b.r3 * scalar;
		Float4 r4 = b.r4 * scalar;

		return Matrix(r1, r2, r3, r4);
	}

	Matrix operator*(const Matrix& b, float scalar) {
		Float4 r1 = b.r1 * scalar;
		Float4 r2 = b.r2 * scalar;
		Float4 r3 = b.r3 * scalar;
		Float4 r4 = b.r4 * scalar;

		return Matrix(r1, r2, r3, r4);
	}
}

