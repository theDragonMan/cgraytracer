#include <cmath>
#include "core/assert.h" 
#include "core/vector.h"
#include "core/point.h"
#include "core/float4.h"


namespace rt{
	const Vector Vector::up = Vector(0, 1.0f, 0);
	const Vector Vector::forward = Vector(0, 0, 1.0f);
	const Vector Vector::right = Vector(1.0f, 0, 0);

	Vector::Vector(const Float4& f4) {
		float d = f4.w == 0 ? 1 : f4.w;
		x = f4.x / d;
		y = f4.y / d;
		z = f4.z / d;
	}

	Vector& Vector::operator=(const Vector& v) {
		if (&v != this) {
			x = v.x;
			y = v.y;
			z = v.z;
		}

		return *this;
	}

	Vector Vector::operator+(const Vector& a) const {
		return Vector(x + a.x, y + a.y, z + a.z);
	}

	Vector Vector::operator-(const Vector& a) const {
		return Vector(x - a.x, y - a.y, z - a.z);
	}

	Vector Vector::normalize() const {
		float len = length();
		return Vector(x / len, y / len, z / len);
	}


	float Vector::lensqr() const {
		return x * x + y * y + z * z;
	}

	float Vector::length() const {
		return sqrt(lensqr());
	}

	Vector Vector::operator-() const {
		return Vector(-x, -y, -z);
	}


	bool Vector::operator==(const Vector& a) const {
		return x == a.x && y == a.y && z == a.z; 
	}

	bool Vector::operator!=(const Vector& a) const {
		return !(x == a.x && y == a.y && z == a.z);
	}

	float& Vector::operator[](int i) {
		assert(i >= 0 && i < 4) << "Index out of range\n";
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
		}
	}

	const float& Vector::operator[](int i) const {
		assert(i >= 0 && i < 4) << "Index out of range\n";
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
		}
	}

	Vector Vector::reflectVector(const Vector& normal, const Vector& v) {
		float cosNormal = dot(normal, v);
		return -v + 2 * cosNormal * normal;
	}

	Vector operator*(float scalar, const Vector& b) {
		return Vector(scalar * b.x, scalar * b.y, scalar * b.z);
	}

	Vector operator*(const Vector& b, float scalar) {
		return Vector(scalar * b.x, scalar * b.y, scalar * b.z);
	}

	Vector operator/(const Vector& b, float scalar) {
		return Vector(b.x / scalar, b.y / scalar, b.z / scalar);
	}

	Vector cross(const Vector& a, const Vector& b) {
		float newX = a.y * b.z - b.y * a.z;
		float newY = a.z * b.x - b.z * a.x;
		float newZ = a.x * b.y - b.x * a.y;
		newX = newX == 0 && std::signbit(newX) ? 0 : newX;
		newY = newY == 0 && std::signbit(newY) ? 0 : newY;
		newZ = newZ == 0 && std::signbit(newZ) ? 0 : newZ;


		return Vector(newX, newY, newZ);
	}

	float dot(const Vector& a, const Vector& b) {
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	Vector min(const Vector& a, const Vector& b) {
		return Vector(fmin(a.x, b.x), fmin(a.y, b.y), fmin(a.z, b.z));
	}

	Vector max(const Vector& a, const Vector& b) {
		return Vector(fmax(a.x, b.x), fmax(a.y, b.y), fmax(a.z, b.z));
	}

	Point operator+(const Vector& a, const Point& p) {
		return Point(p.x + a.x, p.y + a.y, p.z + a.z);
	}

	Point operator+(const Point& p, const Vector& a) {
		return Point(p.x + a.x, p.y + a.y, p.z + a.z);
	}

	Point operator-(const Point& p, const Vector& a) {
		return Point(p.x - a.x, p.y - a.y, p.z - a.z);
	}

	Float4 operator*(const Float4& p, float scale) {
		return Float4(p.x * scale, p.y * scale, p.z * scale, p.w * scale);
	}

	Vector uniformSampleHemisphere(const float& u1, const float& u2)
	{
		// cos(Theta) = u1 = y
		// cos^2(theta) + sin^2(theta) = 1 => sin(theta) = srtf(1 - cos^2(theta))
		float sinTheta = sqrtf(1 - u1 * u1);
		float phi = 2 * pi * u2;
		float x = sinTheta * cosf(phi);
		float z = sinTheta * sinf(phi);

		return Vector(x, u1, z);
	}
}
