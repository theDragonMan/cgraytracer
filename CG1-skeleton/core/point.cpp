#include<algorithm>

#include "core/assert.h"
#include "core/point.h"
#include "core/vector.h"
#include "core/float4.h"



namespace rt {
	using std::min;
	using std::max;

	Point::Point(const Float4& f4) {
		float w = f4.w;
		if(f4.w == 0) w = 1;

		x = f4.x / w;
		y = f4.y / w;
		z = f4.z / w;
	}

	Vector Point::operator-(const Point& a) const {
		return Vector(x - a.x, y - a.y, z - a.z);
	}

	bool Point::operator==(const Point& a) const {
		return x == a.x && y == a.y && z == a.z;
	}

	bool Point::operator!=(const Point& a) const {
		return !(*this == a);
	}

	float& Point::operator[](int i) {
		assert(i >= 0 && i < 4) << "Index out of range\n";
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
		}
	}

	const float& Point::operator[](int i) const {
		assert(i >= 0 && i < 4) << "Index out of range\n";
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
		}
	}


	Point operator*(float scalar, const Point& p) {
		return Point(scalar * p.x, scalar * p.y, scalar * p.z);
	}

	Point operator*(const Point& p, float scalar) {
		return Point(scalar * p.x, scalar * p.y, scalar * p.z);
	}


	Point min(const Point& a, const Point& b) {
		return Point(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z));
	}

	Point max(const Point& a, const Point& b) {
		return Point(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z));
	}
}