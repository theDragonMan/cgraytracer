#include "interpolate.h"
#include "assert.h"

namespace rt {

    template <typename T>
    T lerp(const T& px0, const T& px1, float xPoint) {
		//TODO
        T f = (1 - xPoint)* px0 + xPoint * px1;
		return f;
    }

    template <typename T>
    T lerpbar(const T& a, const T& b, const T& c, float aWeight, float bWeight) {
		//TODO
        float cWeight = 1 - aWeight - bWeight;
		return aWeight * a + bWeight * b + cWeight * c;
    }

    template <typename T>
    T lerp2d(const T& px0y0, const T& px1y0, const T& px0y1, const T& px1y1, float xWeight, float yWeight) {
		//TODO
		T f = (1 - xWeight) * (1 - yWeight) * px0y0 +
			  (1 - xWeight) * yWeight * px0y1 +
			  xWeight * (1 - yWeight) * px1y0 +
			  xWeight * yWeight * px1y1;

        return f;
    }

    template <typename T>
    T lerp3d(const T& px0y0z0, const T& px1y0z0, const T& px0y1z0, const T& px1y1z0,
        const T& px0y0z1, const T& px1y0z1, const T& px0y1z1, const T& px1y1z1,
        float xPoint, float yPoint, float zPoint) {

		T f1 =  (1 - xPoint) * (1 - yPoint) * px0y0z0 +
				(1 - xPoint) * yPoint * px0y1z0 +
				xPoint * (1 - yPoint) * px1y0z0 +
				xPoint * yPoint * px1y1z0;

		T f2 = (1 - xPoint) * (1 - yPoint) * px0y0z1 +
			(1 - xPoint) * yPoint * px0y1z1 +
			xPoint * (1 - yPoint) * px1y0z1 +
			xPoint * yPoint * px1y1z1;

        return (1 - zPoint) * f1 + zPoint * f2;
    }

}

