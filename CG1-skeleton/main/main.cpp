#include <SDL.h>
#include "core/assert.h"
#include "rt/renderer.h"

void a_julia();
void a_cameras();
void a_solids();
void a_instancing();
void a_lighting();
void a_materials();
void a_indexing();
void a_textures();
void a_mappers();
void a_distributed();
void a_bumpmappers();
void a_smooth();
void a_normalmappers();
void a_ambient_occlusion();
void a_competition();

bool isRunning = false;

int main(int argc, char* argv[])
{	
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cerr << "SDL could not be initiated: " << SDL_GetError() << std::endl;
		exit(1);
	}


	SDL_Window* window = SDL_CreateWindow("CGRaytacer", 100, 100, 800, 800, SDL_WINDOW_SHOWN);
	if (window == nullptr) {
		std::cerr << "SDL window could not be created: " << SDL_GetError() << std::endl;
		exit(1);
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_SetRenderDrawColor(renderer, 127, 127, 127, 255);
	
	
	isRunning = true;
	SDL_Event e;

	rt::Renderer::sdl_surface = SDL_GetWindowSurface(window);
	rt::Renderer::sdl_window = window;
	rt::Renderer::sdl_renderer = SDL_GetRenderer(window);
	SDL_SetRenderDrawColor(rt::Renderer::sdl_renderer, 0, 0, 0, 255);
	SDL_RenderClear(rt::Renderer::sdl_renderer);
	SDL_RenderPresent(rt::Renderer::sdl_renderer);
	a_competition();
	SDL_Quit();
	
    return 0;
}
