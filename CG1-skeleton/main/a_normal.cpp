
#include <core/assert.h>
#include <core/scalar.h>
#include <core/image.h>
#include <rt/world.h>
#include <rt/renderer.h>

#include <cmath>

#include <rt/groups/simplegroup.h>
#include <rt/cameras/perspective.h>
#include <rt/primmod/nmap.h>
#include <rt/textures/imagetex.h>
#include <rt/solids/triangle.h>
#include <rt/integrators/casting.h>

using namespace rt;

void nmap(Camera* cam, Texture* normaltex, const char* filename) {
    static const float scale = 0.001f;
    Image img(800, 800);
    World world;
    SimpleGroup* scene = new SimpleGroup();
    world.scene = scene;

    

    //floor
    scene->add(
        new NormalMapper(
            new Triangle(Point(000.f,000.f,000.f)*scale, Point(000.f,000.f,560.f)*scale, Point(550.f,000.f,000.f)*scale, nullptr, nullptr),
            normaltex, Point(0.0f,0.0f,0.0f), Point(0.0f, 1.0f, 0.0f), Point(1.0f, 0.0f, 0.0f))
            );
    scene->add(
        new NormalMapper(
            new Triangle(Point(550.f,000.f,560.f)*scale, Point(550.f,000.f,000.f)*scale, Point(000.f,000.f,560.f)*scale, nullptr, nullptr),
            normaltex, Point(1.0f,1.0f,0.0f), Point(1.0f, 0.0f, 0.0f), Point(0.0f, 1.0f, 0.0f))
            ); 

    //ceiling
    scene->add(
        new NormalMapper(
            new Triangle(Point(000.f,550.f,000.f)*scale, Point(550.f,550.f,000.f)*scale, Point(000.f,550.f,560.f)*scale, nullptr, nullptr),
            normaltex, Point(0.0f,0.0f,0.0f), Point(1.0f, 0.0f, 0.0f), Point(0.0f, 1.0f, 0.0f))
        ); 
    scene->add(
        new NormalMapper(
            new Triangle(Point(550.f,550.f,560.f)*scale, Point(000.f,550.f,560.f)*scale, Point(550.f,550.f,000.f)*scale, nullptr, nullptr),
            normaltex, Point(1.0f,1.0f,0.0f), Point(0.0f, 1.0f, 0.0f), Point(1.0f, 0.0f, 0.0f))
        ); 

    //back wall
    scene->add(
        new NormalMapper(
            new Triangle(Point(000.f,000.f,560.f)*scale, Point(000.f,550.f,560.f)*scale, Point(550.f,000.f,560.f)*scale, nullptr, nullptr),
            normaltex, Point(0.0f,0.0f,0.0f), Point(0.0f, 1.0f, 0.0f), Point(1.0f, 0.0f, 0.0f))
        ); 
    scene->add(
        new NormalMapper(
            new Triangle(Point(550.f,550.f,560.f)*scale, Point(550.f,000.f,560.f)*scale, Point(000.f,550.f,560.f)*scale, nullptr, nullptr),
            normaltex, Point(1.0f,1.0f,0.0f), Point(1.0f, 0.0f, 0.0f), Point(0.0f, 1.0f, 0.0f))
            ); 

    //right wall
    scene->add(
        new NormalMapper(
            new Triangle(Point(000.f,000.f,000.f)*scale, Point(000.f,550.f,000.f)*scale, Point(000.f,000.f,560.f)*scale, nullptr, nullptr),
            normaltex, Point(0.0f,0.0f,0.0f), Point(0.0f, 1.0f, 0.0f), Point(1.0f, 0.0f, 0.0f))
        );
    scene->add(
        new NormalMapper(
            new Triangle(Point(000.f,550.f,560.f)*scale, Point(000.f,000.f,560.f)*scale, Point(000.f,550.f,000.f)*scale, nullptr, nullptr),
            normaltex, Point(1.0f,1.0f,0.0f), Point(1.0f, 0.0f, 0.0f), Point(0.0f, 1.0f, 0.0f))
            );

    //left wall
    scene->add(
        new NormalMapper(
            new Triangle(Point(550.f,000.f,000.f)*scale, Point(550.f,000.f,560.f)*scale, Point(550.f,550.f,000.f)*scale, nullptr, nullptr),
            normaltex, Point(0.0f,0.0f,0.0f), Point(1.0f, 0.0f, 0.0f), Point(0.0f, 1.0f, 0.0f))
            ); 
    scene->add(
        new NormalMapper(
            new Triangle(Point(550.f,550.f,560.f)*scale, Point(550.f,550.f,000.f)*scale, Point(550.f,000.f,560.f)*scale, nullptr, nullptr),
            normaltex, Point(1.0f,1.0f,0.0f), Point(0.0f, 1.0f, 0.0f), Point(1.0f, 0.0f, 0.0f))
        ); 

    RayCastingIntegrator integrator(&world);

    Renderer engine(cam, &integrator);
    engine.render(img);
    engine.setSamples(9);
    img.writePNG(filename);
}




void a_normalmappers() {
    ImageTexture* normaltex = new ImageTexture("models/stones_normal.png", ImageTexture::REPEAT, ImageTexture::NEAREST);
    ImageTexture* normaltexsmooth = new ImageTexture("models/stones_normal.png", ImageTexture::REPEAT, ImageTexture::BILINEAR);
    Camera* cam1 = new PerspectiveCamera(Point(0.048f, 0.043f, -0.050f), Vector(-0.3f, -0.3f, 0.7f), Vector(0, 1, 0), 0.686f, 0.686f);
    nmap(cam1, normaltex, "normal-1.png");
    nmap(cam1, normaltexsmooth, "normal-2.png");
    Camera* cam2 = new PerspectiveCamera(Point(0.278f, 0.273f, -0.800f), Vector(0, 0, 1), Vector(0, 1, 0), 0.686f, 0.686f);
    nmap(cam2, normaltexsmooth, "normal-3.png");
}