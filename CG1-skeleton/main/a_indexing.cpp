
#include <core/assert.h>
#include <core/scalar.h>
#include <core/image.h>
#include <rt/world.h>
#include <rt/renderer.h>
#include <rt/loaders/obj.h>
#include <rt/groups/bvh.h>
#include <rt/groups/kdtree.h>
#include <rt/groups/simplegroup.h>
#include <rt/solids/sphere.h>
#include <rt/lights/directional.h>
#include <rt/cameras/perspective.h>
#include <rt/integrators/casting.h>
#include <rt/integrators/recraytrace.h>
#include <rt/textures/imagetex.h>
#include <rt/textures/CubeTexture.h>
#include <rt/materials/flatmaterial.h>
#include <rt/coordmappers/EnvironmentMap.h>
#include <rt/solids/InfiniteBox.h>
using namespace rt;

void a_indexing() {
    Image img(800, 600);

    KDTree* scene = new KDTree();
  /* scene->add(new Sphere(Point(2.5f,  .5f,  -1), 0.5  , nullptr, nullptr));
    scene->add(new Sphere(Point(2.5f,  -1.f,  -1), 0.5, nullptr, nullptr));
    scene->add(new Sphere(Point(4.5f,  .5f,  -1), 0.5 , nullptr, nullptr));
*/
    loadOBJ(scene, "models/Batman/", "Batman_Injustice.obj");
  
	scene->rebuildIndex();
    World world;
    world.scene = new SimpleGroup();

	ImageTexture* posX = new ImageTexture("models/posx.png");
	ImageTexture* negX = new ImageTexture("models/negx.png");
	ImageTexture* posY = new ImageTexture("models/posy.png");
	ImageTexture* negY = new ImageTexture("models/negy.png");
	ImageTexture* posZ = new ImageTexture("models/posz.png");
	ImageTexture* negZ = new ImageTexture("models/negz.png");
	CubeTexture* skycube = new CubeTexture(posX, negX, posY, negY, posZ, negZ);
	FlatMaterial* envMaterial = new FlatMaterial(skycube);

	EnvironmentMapper* envMap = new EnvironmentMapper();
	InfiniteBox* infiniteBox = new InfiniteBox(envMap, envMaterial);

	((SimpleGroup*)world.scene)->add(scene);
	((SimpleGroup*)world.scene)->add(infiniteBox);


    PerspectiveCamera cam1(Point(-9.85f, -8.85f, 9.4f), Vector(1.0f,1.0f,-0.6f), Vector(0, 0, 1), pi/8, pi/6);
    PerspectiveCamera cam2(Point(16.065f, -12.506f, 1.771f), Point(-0.286f, -0.107f, 1.35f)-Point(16.065f, -12.506f, 1.771f), Vector(0, 0, 1), pi/8, pi/6);
	DirectionalLight* directionalLight = new DirectionalLight(Vector(0, 0, -1), RGBColor(1, 1, 1));
	world.light.push_back(directionalLight);
	img.clear(RGBColor(0, 0, 0));
	RecursiveRayTracingIntegrator integrator(&world);
	Renderer engine1(&cam1, &integrator);
    engine1.render(img);
	img.writePNG("a3-1.png");
	//img.clear(RGBColor(0, 0, 0));
 //   Renderer engine2(&cam2, &integrator);
 //   engine2.render(img);
 //   img.writePNG("a3-2.png");

	
	delete skycube;
	delete envMaterial;
	delete envMap;
	delete infiniteBox;

}
